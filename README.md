# Psono Documentation

[![Discord](https://img.shields.io/badge/Discord-join%20chat-738bd7.svg)](https://discord.gg/VmBMzTSbGV)

# Canonical source

The canonical source of Psono server is [hosted on GitLab.com](https://gitlab.com/psono/psono-doc).

# Preamble

Psono docs are driven by Vuepress

This whole guide is based on Ubuntu 18.04 LTS. Ubuntu 12.04+ LTS and Debian based systems should be similar if not even
identical.

# General

The current build can be found here:

https://doc.psono.com/

# Installation

To install all dependencies run the following command:

	npm ci
	npm i -g raml2html raml2html-slate-theme

# Start the development server

To start the development server execute the following command:

	npm run docs:dev

# Create build

To build the production build execute the following command:

	npm run docs:build

# LICENSE

Visit the [License.md](/LICENSE.md) for more details