module.exports = {
  title: 'Psono Documentation',
  description: 'These brief instructions will help you get started quickly with the Psono password manager',
  locales: {
    '/': {
      lang: 'en-US',
      title: 'Psono Documentation',
      description: 'These brief instructions will help you get started quickly with the Psono password manager.'
    }
  },
  plugins: {
    'sitemap': {
      hostname: 'https://doc.psono.com'
    },
    'vuepress-plugin-code-copy': true,
  },
  markdown: {
    'externalLinks': { target: '_blank', rel: 'noopener noreferrer nofollow' },
  },
  head: [
    ['script', {src: 'https://doc.psono.com/a.js'}],
    // ['script', {}, `
    //   const GA_ID = 'UA-85002864-2'
    //   window.ga =
    //           window.ga ||
    //           function () {
    //             if (!GA_ID) {
    //               return
    //             }
    //             ;(ga.q = ga.q || []).push(arguments)
    //           }
    //   ga.l = +new Date()
    //   ga('create', GA_ID, {
    //     'storage': 'none',
    //     'anonymizeIp': true
    //   });
    //   ga('set', 'transport', 'beacon')
    //   var timeout = setTimeout(
    //           (onload = function () {
    //             clearTimeout(timeout)
    //             ga('send', 'pageview')
    //           }),
    //           1000,
    //   )
    //
    // `]
  ],
  themeConfig: {
    logo: '/images/logo.png',
    smoothScroll: true,
    repo: 'https://gitlab.com/psono',
    // if your docs are in a different repo from your main project:
    docsRepo: 'https://gitlab.com/psono/psono-doc',
    // if your docs are not at the root of the repo:
    docsDir: 'docs',
    // if your docs are in a specific branch (defaults to 'master'):
    docsBranch: 'develop',
    // defaults to false, set to true to enable
    editLinks: true,
    // custom text for edit link. Defaults to "Edit this page"
    editLinkText: 'Edit this page',
    algolia: {
      apiKey: '4534c70b30b91dbf2dfc9da0bfa3ff64',
      indexName: 'psono',
      appId: 'U9PPFWQ4YY'
    },
    locales: {
      '/': {
        selectText: 'Languages',
        label: 'English',
        serviceWorker: {
          updatePopup: {
            message: "New content is available.",
            buttonText: "Refresh"
          }
        },
        nav: [
          { text: 'Home', link: '/' },
          {
            text: 'More',
            ariaLabel: 'More',
            items: [
              {
                text: 'Users:',
                items: [
                  {
                    text: 'Getting Started',
                    link: '/user/getting-started/overview'
                  },
                  {
                    text: 'Basics',
                    link: '/user/basic/overview'
                  },
                  {
                    text: 'Api Key',
                    link: '/user/api-key/overview'
                  },
                  {
                    text: 'Groups',
                    link: '/user/groups/creation'
                  },
                  {
                    text: 'File Repository',
                    link: '/user/file-repository/overview'
                  },
                  {
                    text: 'Two Factor Authentication',
                    link: '/user/two-factor-authentication/duo'
                  },
                  {
                    text: 'Other',
                    link: '/user/other/autofill-extensions'
                  }
                ]
              },
              {
                text: 'Admins:',
                items: [
                  {
                    text: 'Summary',
                    link: '/admin/overview/summary'
                  },
                  {
                    text: 'Installation',
                    link: '/admin/installation/install-preparation'
                  },
                  {
                    text: 'Configuration',
                    link: '/admin/configuration/audit-log'
                  },
                  {
                    text: 'Update',
                    link: '/admin/update/update-psono-ce'
                  },
                  {
                    text: 'FAQ',
                    link: '/admin/faq/faq'
                  },
                  {
                    text: 'ASVS',
                    link: '/admin/asvs/overview'
                  },
                  {
                    text: 'Other',
                    link: '/admin/other/commands'
                  }
                ]
              },
              {
                text: 'Developer:',
                items: [
                  {
                    text: 'General',
                    link: '/admin/development/contribution-agreement'
                  },
                  { text: 'API', link: 'https://doc.psono.com/api.html' }
                ]
              },
              {
                text: 'Console Users:',
                items: [
                  {
                    text: 'Psono SaaS Instance',
                    link: '/console/psono-saas-instance/creation'
                  }
                ]
              }
            ]
          },
          { text: 'Legal Notice', link: '/legal-notice' },
          { text: 'Privacy Policy', link: '/privacy-policy' },
          { text: 'Psono.com', link: 'https://psono.com/' },
          { text: 'Discord', link: 'https://discord.gg/VmBMzTSbGV' }
        ],
        sidebar: {
          '/admin/': [
            {
              title: 'Overview',
              collapsable: false,
              children: [
                'overview/summary',
                'overview/introduction',
                'overview/supported-features',
                'overview/about',
                'overview/support'
              ]
            },
            {
              title: 'Installation',
              collapsable: false,
              children: [
                'installation/install-preparation',
                'installation/install-postgres-db',
                'installation/install-psono-ce',
                'installation/install-psono-ee',
                'installation/install-reverse-proxy',
                'installation/install-finalize'
              ]
            },
            {
              title: 'Installation (optional)',
              collapsable: false,
              children: [
                'installation-optional/install-browser-extension',
                'installation-optional/install-mobile-app',
                'installation-optional/install-fileserver'
              ]
            },
            {
              title: 'Configuration',
              collapsable: false,
              children: [
                'configuration/audit-log',
                'configuration/audit-log-splunk',
                'configuration/audit-log-s3',
                'configuration/audit-log-logstash',
                'configuration/browser-extension',
                'configuration/compliance-settings',
                'configuration/custom-branding',
                'configuration/email-amazon-ses',
                'configuration/email-mailgun',
                'configuration/email-mailjet',
                'configuration/email-mandrill',
                'configuration/email-postmark',
                'configuration/email-sendgrid',
                'configuration/email-brevo',
                'configuration/email-smtp',
                'configuration/email-sparkpost',
                'configuration/intune',
                'configuration/ldap-ad',
                'configuration/ldap-freeipa',
                'configuration/ldap-openldap',
                'configuration/ldaps-custom-ca',
                'configuration/ldap-group-mapping',
                'configuration/ldap-extras',
                'configuration/ldap-gateway',
                'configuration/oidc-cidaas',
                'configuration/oidc-google-identity',
                'configuration/oidc-keycloak',
                'configuration/oidc-wso2-identity-server',
                'configuration/oidc-zitadel',
                'configuration/oidc-group-mapping',
                'configuration/oidc-extras',
                'configuration/policies',
                'configuration/scim',
                'configuration/scim-group-mapping',
                'configuration/saml-adfs',
                'configuration/saml-authentik',
                'configuration/saml-aws',
                'configuration/saml-azure-ad',
                'configuration/saml-google-workspace',
                'configuration/saml-keycloak',
                'configuration/saml-okta',
                'configuration/saml-univention',
                'configuration/saml-group-mapping',
                'configuration/saml-extras',
                'configuration/throttle-rates',
                'configuration/two-factor-ivalt'
              ]
            },
            {
              title: 'Update',
              collapsable: false,
              children: [
                'update/update-psono-ce',
                'update/update-psono-ee',
                'update/update-fileserver',
              ]
            },
            {
              title: 'Development',
              collapsable: false,
              children: [
                'development/contribution-agreement',
                'development/server',
                'development/webclient',
                'development/browser-extension',
                'development/entity-model',
                'development/datastore-structure',
                'development/cryptography',
                'development/build-pipeline'
              ]
            },
            {
              title: 'ASVS',
              collapsable: false,
              children: [
                'asvs/overview',
                'asvs/v01-architecture',
                'asvs/v02-authentication',
                'asvs/v03-session-management',
                'asvs/v04-access-control',
                'asvs/v05-malicious-input',
                'asvs/v06-output-encoding',
                'asvs/v07-cryptography',
                'asvs/v08-error-handling',
                'asvs/v09-data-protection',
                'asvs/v10-communications',
                'asvs/v11-http-security',
                'asvs/v12-security-configuration',
                'asvs/v13-malicious-controls',
                'asvs/v14-internal-security',
                'asvs/v15-business-logic-flaws',
                'asvs/v16-files-and-resources',
                'asvs/v17-mobile',
                'asvs/v18-web-services',
                'asvs/v19-configuration',
                'asvs/stride'
              ]
            },
            {
              title: 'FAQ',
              collapsable: false,
              children: [
                'faq/faq'
              ]
            },
            {
              title: 'Other',
              collapsable: false,
              children: [
                'other/commands',
                'other/environment-variables',
                'other/docker-secrets',
                'other/healthcheck'
              ]
            },
            {
              title: 'Installation (old)',
              children: [
                'installation-old/install-postgres-db',
                'installation-old/install-server-ce',
                'installation-old/install-server-ee',
                'installation-old/install-webclient',
                'installation-old/install-admin-webclient',
                'installation-old/install-reverse-proxy'
              ]
            },
            {
              title: 'Update (old)',
              children: [
                'update-old/update-server-ce',
                'update-old/update-server-ee',
                'update-old/update-webclient',
                'update-old/update-admin-client',
                'update-old/update-fileserver',
              ]
            }
          ],
          '/user/': [
            {
              title: 'Getting started',
              collapsable: false,
              children: [
                'getting-started/overview',
                'getting-started/features'
              ]
            },
            {
              title: 'Basics',
              collapsable: false,
              children: [
                'basic/overview',
                'basic/creating-folders',
                'basic/creating-secrets',
                'basic/searching',
                'basic/sharing',
                'basic/moving',
                'basic/deleting'
              ]
            },
            {
              title: 'API Key',
              collapsable: false,
              children: [
                'api-key/overview',
                'api-key/creation',
                'api-key/usage-with-session',
                'api-key/usage-without-session-and-local-decryption',
                'api-key/usage-without-session-and-remote-decryption'
              ]
            },
            {
              title: 'Groups',
              collapsable: false,
              children: [
                'groups/creation',
                'groups/leaving',
                'groups/deletion',
                'groups/adding-users',
                'groups/removing-users',
                'groups/modifying-permissions',
              ]
            },
            {
              title: 'File Repository',
              collapsable: false,
              children: [
                'file-repository/overview',
                'file-repository/setup-aws-as-file-repository',
                'file-repository/setup-azure-blob-storage-as-file-repository',
                'file-repository/setup-do-as-file-repository',
                'file-repository/setup-gcs-as-file-repository',
                'file-repository/setup-other-s3-storage-file-repository',
                'file-repository/share-file-repositories'
              ]
            },
            {
              title: 'Two Factor Authentication',
              collapsable: false,
              children: [
                'two-factor-authentication/duo',
                'two-factor-authentication/fido2-webauthn',
                'two-factor-authentication/google-authenticator',
                'two-factor-authentication/ivalt',
                'two-factor-authentication/yubikey'
              ]
            },
            {
              title: 'psonoci for CI / CD',
              collapsable: false,
              children: [
                'psonoci/install',
                'psonoci/usage'
              ]
            },
            {
              title: 'Other',
              collapsable: false,
              children: [
                'other/autofill-extensions',
                'other/custom-fields',
                'other/delete-account',
                'other/disable-browser-password-manager',
                'other/emergency-codes',
                'other/export',
                'other/import',
                'other/offline-mode',
                'other/recovery-codes',
                'other/password-capture',
                'other/remote-desktop-manager'
              ]
            }
          ],
          '/console/': [
            {
              title: 'Psono SaaS Instance',
              collapsable: false,
              children: [
                'psono-saas-instance/creation',
                'psono-saas-instance/configuration',
              ]
            }
          ],
          '/': [
            {
              title: 'Home',
              collapsable: false,
              children: [
                '',
              ]
            }
          ]
        }
      }
    }
  },
  dest: './build',
};