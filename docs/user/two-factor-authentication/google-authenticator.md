---
title: Google Authenticator
metaTitle: Setup Google Authenticator | Psono Documentation
meta:
  - name: description
    content: How to protect your account with Google Authenticator
---

# Setup Google Authenticator

## Preamble

The completely free alternative to other 2-Factor Authenticators is the Google Authenticator.
We strongly recommend setting up such a 2-Factor Authentication to protect your account.
Please generate a recovery code afterward so you always have a possibility to gain access to your account in case
you lose your mobile.

## Compatible Apps

The Google Authenticator is an "open standard", that has been implemented by a couple of different Apps / Vendors.
Psono supports all compatible Apps, including the following:

* Google Authenticator (the original) (for [Android](https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2) and [Apple](https://itunes.apple.com/de/app/google-authenticator/id388497605?mt=8))
* Authy (with a backup function (for [Android](https://play.google.com/store/apps/details?id=com.authy.authy) and [Apple](https://itunes.apple.com/de/app/authy/id494168017?mt=8))

::: warning
Be careful if you use other maybe not so trustworthy alternatives.
:::

## Setup Guide

1)  Login into your account:

![Step 1 Login](/images/user/basic/step1-login.jpg)

2)  Go to "Account":

![Step 2 go to account](/images/user/basic/step2-go-to-account.jpg)

3)  Select the “Multifactor Authentication” tab:

![Step 3 go to multifactor authentication](/images/user/2fa_yubikey/step3-go-to-multifactor-authentication.jpg)

4)  Click the "Configure" button next to Google Authenticator:

![Step 4 click configure next to Google Authenticator](/images/user/2fa_google_authenticator/step4-click-configure-next-to-google-authenticator.jpg)

5)  Click the "New GA" tab:

![Step 5 select new Yubikey](/images/user/2fa_google_authenticator/step5-click-new-ga.jpg)

6)  Type in some descriptive text to identify your Google Authenticators later:

![Step 6 some descriptive title](/images/user/2fa_google_authenticator/step6-some-descriptive-title.jpg)

7)  Scan the QR Code with your App:

![Step 7 scan qr code](/images/user/2fa_google_authenticator/step7-scan-qr-code.jpg)

8) Enter one valid Google Authenticator code:

![Step 8 one valid Google Authenticator code](/images/user/2fa_google_authenticator/step8-enter-google-authenticator-code.jpg)

Well done, your first Google Authenticator is now active. We strongly recommend setting up a recovery code in case your phone gets damaged, is lost or stolen.

