---
title: iVALT
metaTitle: Setup iVALT two-factor-authentication | Psono Documentation
meta:
  - name: description
    content: How to protect your account with iVALT two-factor-authentication
---

# Setup iVALT two-factor-authentication

## Preamble

Another well-known two-factor-authenticator is iVALT. We strongly recommend setting up such a 2-Factor Authentication to
protect your account. Please generate a recovery code afterward so you always have a possibility to gain access to your
account in case you lose your mobile.

## Pre-requirements

During the registration process you are "forced" to download the "iVALT" App, that you can get here for
[Android](https://play.google.com/store/apps/details?id=com.abisyscorp.ivalt) or here for
[Apple](https://apps.apple.com/us/app/ivalt/id1507945806). Your administrator also needs to configure iVALT first
according to [Base setup of iVALT two factor](/admin/configuration/two-factor-ivalt.html).

## Setup Guide

1)  Login into your account:

    ![Step 1 Login](/images/user/basic/step1-login.jpg)

2)  Go to "Account":

    ![Step 2 go to account](/images/user/2fa_ivalt/step2-go-to-account.png)

3)  Select the “Multifactor Authentication” tab:

    ![Step 3 go to multifactor authentication](/images/user/2fa_ivalt/step3-go-to-multifactor-authentication.png)

4)  Click the "Configure" button next to "iVALT":

    ![Step 4 click configure next to iVALT Authenticator](/images/user/2fa_ivalt/step4-click-configure-next-to-ivalt-authenticator.png)

5)  Click the "+" icon:

    ![Step 5 Click the plus icon](/images/user/2fa_ivalt/step5-click-plus-icon.png)

6)  Select your country and then enter your registered mobile number:

    ![Step 6 click setup](/images/user/2fa_ivalt/step6-click-setup.png)

7)  Now you will receive authentication notification on your iVALT mobile app. Approve the notification:

    ![Step 7 waiting screen](/images/user/2fa_ivalt/step7-waiting-screen.png)

8)  After successful verification:

    ![Step 8 2fa added](/images/user/2fa_ivalt/step8-ivalt-2fa-added.png)

Well done, your iVALT MFA is now active. We strongly recommend setting up a recovery code in case your phone gets damaged, is lost or stolen.

