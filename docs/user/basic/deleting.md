---
title: Deleting
metaTitle: Deleting secrets and folders | Psono Documentation
meta:
  - name: description
    content: A small guide to explain how to delete secrets and folders and their restoration
---

# Deleting secrets and folders and the restoration

This section will explain how to delete one or more entry (secret or folder) and how to restore them later from the trash bin.


## Deleting a single entry


1.  Deletion of an entry
    
    You can delete an entry either by "right click"-ing the entry or by clicking
    on the three gears button of the entry. Click on `Move to trash`
    
    ![Move to trash bin](/images/user/basic/move-to-trash-bin.png)

    The entry was now moved to the trash bin. If the option isn't visible you may not have
    the necessary permissions

## Deleting multiple entries


1. Hold "Shift"

2. Use the checkboxes to select multiple entries

   ![Use checkboxes to select entries](/images/user/basic/mass-operation-checkbox.png)

   ::: tip
   No folder and only entries of the same parent (e.g. share or datastore) can be deleted like that.
   :::

3. Use the delete button to delete the entries

   ![Click mass delete button](/images/user/basic/mass-operation-delete.png)


## Restoration

If you delete an entry by accident, then you need to know that it's not directly deleted and instead just moved to the
trash bin. You may restore as such any entry that was deleted by accident.

1.  Open Trash bin
    
    You can access your trash bin by clicking on the small trash bin symbol at the top right corner of your datastore
    screen:

    ![Open trash bin](/images/user/basic/open-trash-bin.png)

2.  Click Restore
    
    In the restore column click the restore symbol.

    ![link share settings](/images/user/basic/restore-from-trash-bin.png)

    The entry should now re-appear in your regular datastore.


## Permanently delete

You may want to delete an entry from your datastore completely. As such move it to the trash bin, as described above
and then 

1.  Open Trash bin

    You can access your trash bin by clicking on the small trash bin symbol at the top right corner of your datastore
    screen:

    ![Open trash bin](/images/user/basic/open-trash-bin.png)

2.  Click Delete
    
    In the right column click the delete symbol.

    ![link share settings](/images/user/basic/delete-from-trash-bin.png)

    The entry should now be removed from your trash bin.

    ::: tip
    Please take note that the deletion only affects your datastore. If the entry (or any sub entry) was shared with 
    another user or group, then the other user or group will still have access to it.
    :::