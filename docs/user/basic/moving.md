---
title: Moving
metaTitle: Moving secrets and folders | Psono Documentation
meta:
  - name: description
    content: A small guide to explain how to move secrets and folders
---

# Move secrets and folders

This section will explain how to move one or more entries (secret or folder).


## Moving a single entry


1.  Initiate move
    
    You can move an entry either by "right click"-ing the entry or by clicking
    on the three gears button of the entry. Click on `Move`
    
    ![Move](/images/user/basic/move.png)

2.  Choose destination
    
    Select now the destination where oyu want to move the entry to.
    
    ![Move](/images/user/basic/move-entry-choose-new-folder.png)

    ::: tip
    Certain entries may be grayed out as you don't have the necessary permission.
    :::

3.  Confirm your choice

    Click on the `OK` button to confirm your choice.

## Moving multiple entries

1. Hold "Shift"

2. Use the checkboxes to select multiple entries

   ![Use checkboxes to select entries](/images/user/basic/mass-operation-checkbox.png)

   ::: tip
   No folder and only entries of the same parent (e.g. share or datastore) can be moved like that.
   :::

3. Use the move button to initiate the move of the entries

   ![Click mass move button](/images/user/basic/mass-operation-move.png)

4.  Choose destination

    Select now the destination where oyu want to move the entry to.

    ![Move](/images/user/basic/move-entry-choose-new-folder.png)

    ::: tip
    Certain entries may be grayed out as you don't have the necessary permission.
    :::

5.  Confirm your choice

    Click on the `OK` button to confirm your choice.
