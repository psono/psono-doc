---
title: Overview
metaTitle: Basic Overview | Psono Documentation
meta:
  - name: description
    content: Overview of how to do basic things like creating, searching and accessing secrets and folders.
---

# Basic Overview

This section will explain you how to use Psono and do most basic things like creating, searching and accessing secrets and folders.

## How to ...

- ... [create folders](/user/basic/creating-folders.html)
- ... [create secrets](/user/basic/creating-secrets.html)
- ... [search secrets and folders](/user/basic/searching.html)
- ... [share secrets and folders](/user/basic/sharing.html)
- ... [delete secrets and folders](/user/basic/deleting.html)
