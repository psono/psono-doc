---
title: Sharing
metaTitle: Sharing secrets and folders | Psono Documentation
meta:
  - name: description
    content: A small guide to explain how to share secrets and folders with other users
---

# Sharing secrets (Shares)

This section will explain how to share a particular entry (secret or folder) with other users. There are multiple ways
to share an entry (secret or folder), depending if the other user has an account or not

## With another Psono user

This method describes how to share entries with other users that have a Psono account.

1.  Initiate the sharing
    
    If the other user has a Psono account too, then you can share an entry either "right click"-ing the entry or by clicking
    on the three gears button of the entry. Click on `Share`
    
    ![Initiate sharing](/images/user/basic/initiate-sharing.png)

1.  Select the appropriate permissions


    ![sharing permissions](/images/user/basic/sharing-permissions.png)
    
    
    - Read: Allows a user to read the content of a share
    - Write: Allows a user to modify the content of a share
    - Grant: Allows the user to modify the access permissions (including his own) and share it with other users / groups.
    

1.  Select (or search) the user(s)

    
    ![select user](/images/user/basic/select-user.png)
    
    If you never shared anything with the desired user, the user won't be listed here. You can ask the server for the user
    by clicking the `+`-Symbol.
    
    
    ![search user](/images/user/basic/search-user.png)

1.  Finally create the share
    
    Finish the process by clicking `OK` at the bottom
    
    
    ![finish sharing user](/images/user/basic/finish-sharing-user.png)
    
    
    You will notice that the icon of the entry has changed and this little green mark appeared. This indicates that the 
    entry does not live anymore directly in your datastore and instead in a so called "share object" that may be shared with 
    others and has own share permissions.
    
    ![finish sharing user](/images/user/basic/icon-change-share.png)
    
    ::: tip
    We recommend to share folders instead of secrets, as they allow other people to receive new secrets and subfolders without having to accept
    them one by one. In addition it makes audits easier. Further we recommend to share entries based on groups instead of single
    users. E.g. so all people in the marketing department have access to a particular entry and not just user XY.
    :::

## With externals (Link Shares)

This method describes how to share entries with external parties, that don't have a Psono account yet can (network, firewall wise)
reach the Psono server. It tries to solve the problem of sharing secrets securely without them ending up in emails or chats.


1.  Initiate a link share
    
    You start the process by a right click on the entry or by clicking on the three gears button next to the entry and then
    clicking on `Link Share`

    ![initiate link share](/images/user/basic/initiate-link-share.png)

2.  Specify link share details
    
    You can now specify a public title shown to the user, the amount how often the link can be used, a time until the link
    expires or a passphrase that needs to be typed in in addition to access the link share.

    ![link share settings](/images/user/basic/link-share-settings.png)

    Once you are happy with the settings, confirm the dialog with `OK`.

3.  Copy link
    
    You will see now a link that you can copy and share with others.

    ![link share url](/images/user/basic/link-share-final-step.png)
    
    You can distribute the link by mail or chat and will be sure that the link expires or can only be used maybe once,
    so everyone who might gain access to your emails or chat logs later will not have access to the secret.