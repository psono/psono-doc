---
title: Leaving
metaTitle: Leaving a group | Psono Documentation
meta:
  - name: description
    content: Instructions how to leave g group
---

# Leaving

## Preamble

To leave a group, you must have an account and be logged in with a browser client.

## Guide

1)  In the left menu, click on "Groups":

![Step 1 Go to Groups](/images/user/groups/step-1-go-to-groups.png)

2)  Click the leave icon to leave the group:

![Step 2 leave group](/images/user/groups/step-2-leave-group.png)

