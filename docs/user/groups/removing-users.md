---
title: Removing users
metaTitle: Removing users | Psono Documentation
meta:
  - name: description
    content: Instructions how to remove a user from a group
---

# Removing users

## Preamble

To remove a user from a group, you must have an account and be logged in with a browser client.
You also need "Group Admin" permissions.

## Guide

1)  In the left menu, click on "Groups":

![Step 1 Go to Groups](/images/user/groups/step-1-go-to-groups.png)

2)  Edit the group:

Click on the pencil icon to edit the group

![Step 2 Edit group](/images/user/groups/step-2-edit-group-with-pencil.png)

3)  Uncheck the Member checkbox next to the user

![Step 3 uncheck Member checkbox](/images/user/groups/step-3-uncheck-member-checkbox.png)

4)  Click "Save"
