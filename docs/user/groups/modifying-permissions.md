---
title: Modifying permissions
metaTitle: Modifying user permissions for a group | Psono Documentation
meta:
  - name: description
    content: Instructions how to modify a user's permissions of a groups
---

# Modifying permissions

## Preamble

To remove a user from a group, you must have an account and be logged in with a browser client.
You also need "Group Admin" permissions.

## Guide

1)  In the left menu, click on "Groups":

![Step 1 Go to Groups](/images/user/groups/step-1-go-to-groups.png)

2)  Edit the group:

Click on the pencil icon to edit the group

![Step 2 Edit group](/images/user/groups/step-2-edit-group-with-pencil.png)

3)  Select the correct permissions

![Step 3 select permissions](/images/user/groups/step-3-select-permissions.png)

- Group Admin: Allows a user to add and remove users and their permissions from a group
- Share Admin: Allows a user to share an entry or folder with this group


4)  Click "Save"
