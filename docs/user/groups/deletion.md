---
title: Deletion
metaTitle: Deletion of groups | Psono Documentation
meta:
  - name: description
    content: Instructions how to delete groups
---

# Deletion

## Preamble

To delete groups as a user (so-called "unmanaged" groups), you must have an account and be logged in with a browser client. You also need "Group Admin" permissions.

## Guide

1)  In the left menu, click on "Groups":

![Step 1 Go to Groups](/images/user/groups/step-1-go-to-groups.png)

2)  Click the trash bin icon to delete the group:

![Step 2 delete group](/images/user/groups/step-2-delete-group.png)

