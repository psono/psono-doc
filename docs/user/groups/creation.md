---
title: Creation
metaTitle: Creation of groups | Psono Documentation
meta:
  - name: description
    content: Instructions how to create groups
---

# Creation

## Preamble

To create groups as a user (called "unmanaged" groups), you must have an account and be logged in with a browser client.

## Guide

1)  In the left menu, click on "Groups":

![Step 1 Go to Groups](/images/user/groups/step-1-go-to-groups.png)

2)  Click on the "+" icon to add a group:

![Step 2 Go to Groups](/images/user/groups/step-1-click-plus.png)

3)  Enter the name of the group:

![Step 3 Fill name](/images/user/groups/step-2-fill-in-name.png)

4)  Click "Create"
