---
title: Adding users
metaTitle: Adding users | Psono Documentation
meta:
  - name: description
    content: Instructions how to add users to a groups
---

# Adding users

## Preamble

To add a user to a group as a user (to a so-called "unmanaged" group), you must have an account and be logged in with a browser client.
You also need "Group Admin" permissions.

## Guide

1)  In the left menu, click on "Groups":

![Step 1 Go to Groups](/images/user/groups/step-1-go-to-groups.png)

2)  Edit the group:

Click on the pencil icon to edit the group

![Step 2 Edit group](/images/user/groups/step-2-edit-group-with-pencil.png)

3)  Click "On plus symbol" to search a user

If you don't see the user in the list of users, you need to find them on the server. Click the "+" symbol and search for the user.

![Step 3 Add user](/images/user/groups/step-3-add-users.png)

4)  Select the user and permissions

Select the user to invite the user to the group. Click on the checkboxes to configure the user's permissions for the group.

![Step 4 select permissions](/images/user/groups/step4-select-permissions.png)

- Group Admin: Allows a user to add and remove users and their permissions from a group
- Share Admin: Allows a user to share an entry or folder with this group

5)  Click "Save"
