---
title: Custom Fields
metaTitle: Custom Fields | Psono Documentation
meta:
  - name: description
    content: How to create custom fields and use auto fill
---

# Custom Fields

This guide explains how to create, update and delete custom fields. It will also explain how one can configure to autofill these custom fields in forms later.

## Create custom fields

To create a new custom field follow the the guide below:

1)  Login into your account:
    
    ![Step 1 Login](/images/user/basic/step1-login.jpg)

2)  Edit entry:

    Click on the entry to edit it

    ![Edit entry](/images/user/other_custom_fields/custom_fields_01_click_on_entry.png)

    ::: tip
    Only website passwords, application passwords, bookmarks and notes support custom fields. 
    :::

3)  Add custom field:

    Click on "Add" and then "Add custom field" to add a custom field

    ![Add custom field](/images/user/other_custom_fields/custom_fields_02_add_custom_entry.png)

4)  Configure custom field:

    Choose a name and an appropriate type and hit `Save`.

    ![Configure custom field](/images/user/other_custom_fields/custom_fields_03_configure_custom_field.png)

    ::: tip
    The name of the entry will later be used to match entries during autofill. The matching is done by the DOM's input element properties, namely its `id`, `name`, `aria-label`, `placeholder` or `label` (in that order).
    :::

5)  Add a value:

    Enter the value that you'd like to store with the custom field.

    ![Enter value of the custom field](/images/user/other_custom_fields/custom_fields_04_enter_value_for_custom_field.png)

## Modify custom fields

You may want to change the name or type of a custom field.

1)  Login into your account:

    ![Step 1 Login](/images/user/basic/step1-login.jpg)

2)  Edit entry:

    Click on the entry to edit it

    ![Edit entry](/images/user/other_custom_fields/custom_fields_01_click_on_entry.png)

3)  Modify custom field:

    Choose the custom field that you'd like to edit. Use the burger menu on the right to open the menu and choose `Edit custom field`

    ![Modify custom field](/images/user/other_custom_fields/custom_fields_05_modify_custom_field.png)

4)  Configure custom field:

    Configure the custom field accordingly and click `Save`

    ![Configure custom field](/images/user/other_custom_fields/custom_fields_06_configure_custom_field.png)

## Delete custom fields

If you may want to delete a custom field you can follow the guide below:

1)  Login into your account:

    ![Step 1 Login](/images/user/basic/step1-login.jpg)

2)  Edit entry:

    Click on the entry to edit it

    ![Edit entry](/images/user/other_custom_fields/custom_fields_01_click_on_entry.png)

3)  Modify custom field:

    Choose the custom field that you'd like to edit. Use the burger menu on the right to open the menu and choose `Remove custom field`

    ![Modify custom field](/images/user/other_custom_fields/custom_fields_07_remove_custom_field.png)