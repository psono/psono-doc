---
title: Remote Desktop Manager
metaTitle: Remote Desktop Manager | Psono Documentation
meta:
  - name: description
    content: How to integrate and use Remote Desktop Manager together with Psono
---

# Remote Desktop Manager

## Preamble

This guide will explain how to use Remote Desktop Manager together with Psono as a credential datastore.

::: tip
This feature is only available in the "Team Edition" of Remote Desktop Manager and not the "Free Edition".
:::

## Configure Psono

1)  Go to "Other"

![Step 1 Go to Other](/images/user/other_remote_desktop_manager/psono_01_go_to_other.png)

2)  Create new API key

![Step 2 Create new API key](/images/user/other_remote_desktop_manager/psono_02_create_new_API_key.png)

3) API key details

Specify the API key details as seen on the image below.

![Step 3 Specify API key details](/images/user/other_remote_desktop_manager/psono_03_specify_api_key_details.png)

4) Edit the API key

![Step 4 Edit the API key](/images/user/other_remote_desktop_manager/psono_04_edit_api_key.png)

5) Write down the API key details

![Step 5 Take note of the API key details](/images/user/other_remote_desktop_manager/psono_05_copy_details.png)


## Configure Remote Desktop Manager

1)  Click new entry

First we are going to create a new credential entry. Click on New Entry.

![Step 1 Remote Desktop Manager New Entry](/images/user/other_remote_desktop_manager/01_remote_desktop_manager_new_entry.png)

2)  Search Psono

Use the search to search for Psono

![Step 2 search Psono](/images/user/other_remote_desktop_manager/02_remote_desktop_manager_search_psono.png)

3)  Enter API key details

Enter the API key details and check "Always prompt with list"

![Step 3 Enter API key details](/images/user/other_remote_desktop_manager/03_remote_desktop_manager_enter_api_key_details.png)

4)  Create (or edit) an entry

You can now create (or edit) an actual entry, e.g. SSH or RDP. Configure the linked vault and select the one we just
created. Afterwards click on "Select from List" to choose the actual entry.

![Step 4 configure entry](/images/user/other_remote_desktop_manager/04_remote_desktop_manager_configure_linked_vault.png)



