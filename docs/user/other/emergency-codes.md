---
title: Emergency Codes
metaTitle: Emergency Codes | Psono Documentation
meta:
  - name: description
    content: How to setup and use an emergency code
---

# Emergency Codes

## Preamble

In case something is happening to you and you end up in hospital or even less lucky, you die, you might want that
coworkers or close relatives (mother, wife) can access your passwords. That's where "emergency codes" come in.
Emergency codes grant access to your account after waiting a certain amount of time after activation.
Whenever an emergency code is used, an email is sent to your account, allowing you to remove the emergency code
before someone may have accessed your account.

## Setup Emergency Code

1)  Login into your account:

![Step 1 Login](/images/user/basic/step1-login.jpg)

2)  Go to "Account":

![Step 2 go to account](/images/user/basic/step2-go-to-account.jpg)

3)  Select the “Emergency Codes” tab:

![Step 3 Select emergency codes](/images/user/emergency_codes/step3-select-emergency-codes-tab.jpg)

4)  Click "Configure":

![Step 4 Click Configure](/images/user/emergency_codes/step4-click-configure.jpg)

5)  Go to "New Emergency Codes" tab:

![Step 5 Go to the "new Emergency Codes" tab](/images/user/emergency_codes/step5-go-to-new-emergency-codes-tab.jpg)

6)  Enter details:

Add here a name of the person or something "descriptive" for you together with the time that the person should have to
wait, allowing you to step in and deny his try to access your datastore.

![Step 6 Enter details](/images/user/emergency_codes/step6-enter-details-and-click-create.jpg)

7)  Print emergency information

You will see now an output like shown below. Transfer this information to the person that you would like to grant the
possibility in case something happens to you. We recommend to print it and to hand it over in paper.

![Step 7 Print emergency code information](/images/user/emergency_codes/step7-print-information.jpg)

::: warning
This information can grant someone access to your account, so be careful how you handle it.
:::

## Activating an Emergency Code

We assume here that you are in the possession of an emergency code and have to access that person's passwords.

1)  Go to written url:

The recovery information contain an URL. Type it into your browser's URL bar. You should see now a site like this:

![Step 1 Type in URL](/images/user/emergency_codes/step11-go-to-url.jpg)

2)  Enter emergency information:

Your emergency information contain a username a `code` and a `word list`..

You can now enter the code:

![Step 2a Enter Code](/images/user/emergency_codes/step12-enter-code.jpg)

or as an alternative you can enter the word list:

![Step 2b Enter Word List](/images/user/emergency_codes/step13-enter-word-list.jpg)

3)  Click "Activate Emergency Code"

4)  (optional) Wait

It can be that you are not logged in and instead see now a screen as shown below:

![Step 4 wait](/images/user/emergency_codes/step14-wait.jpg)

This means that your emergency code was configured not to grant you instant access and you have to wait a little longer.
Repeat above steps again after the waiting the mentioned amount of time.

