---
title: Import Passwords
metaTitle: Import Passwords | Psono Documentation
meta:
  - name: description
    content: How to import passwords.
---

# Import Passwords

## Preamble

Psono provides the possibility to import the exports from various vendors.

## Supported vendors & formats

| Vendors | Format |
-----------|-----------|---------
| [Psono](https://psono.com) | JSON |
| [1password.com](https://1password.com) | CSV |
| [Bitwarden](https://www.google.com/) | JSON |
| [Chrome](https://chrome.google.com/) | CSV |
| [Enpass](https://www.enpass.io/) | JSON |
| [Firefox](https://www.mozilla.org/firefox/) | CSV |
| [KeePass.info](https://keepass.info/) | CSV |
| [KeePass.info](https://keepass.info/) | XML |
| [KeePassX.org](https://keepassx.org/) | CSV |
| [LastPass](https://lastpass.com/) | CSV |
| [Password Safe](https://passwordsafe.de/) | CSV |
| [Safari](https://www.apple.com/safari/) | CSV |
| [Teampass](https://teampass.net/) | CSV |


## Import


1)  Go to to "other":

![Step 1 Go to Other](/images/user/import/step1-go-to-other.jpg)

2)  Select Type:

![Step 2 select type](/images/user/import/step2-select-type.jpg)

3)  Select File:

![Step 3 select file](/images/user/import/step3-choose-file.jpg)

4)  Click `Export`:

![Step 4 click import](/images/user/import/step4-click-import.jpg)

Your passwords can now be found in your datastore.
