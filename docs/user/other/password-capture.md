---
title: Password Capture
metaTitle: Password Capture | Psono Documentation
meta:
  - name: description
    content: How does Psono's password capture work?
---

# Password Capture

## Preamble

For convenience Psono offers the possibility to capture passwords on the fly and store them in your datastore.

## How does password capture work?

Make sure that you installed the browser extension and that you are logged in. Afterwards the process looks like this:

1)  Login on a website

2)  Accept new entry:

There should be a small browser notification, informing you about the new entry

![Password capture notification](/images/user/password_capture/password_capture_notification.png)

3)  Click on "Yes"

The entry is now stored in your datastore


## Potential issues

Under the hood the mechanism tries first to identify login forms and then to hook into the submit event to know when you submitted the form.
If you epxperience an issue, it eather means that Psono couldn't properly detect that this is a login form or that the website uses an unsupported submit mechanism.
Even so we try to support a broad range of forms, it is sadly not possible to support all websites. If you find a website that doesn't work feel free to reach out and we take a look if we can 
create a fix for it.