---
title: Offline Mode
metaTitle: Offline Mode | Psono Documentation
meta:
  - name: description
    content: How does Psono's Offline Mode work?
---

# Offline Mode

## Preamble

Psono allows you to take your vault offline. The offline mode is intended for times when you know that you need access
to your vault at a time when you don't have any internet connection. A typical example would be a technician who visits
a customer's site where he has no access to the corporate network. The offline mode is a read only mode where a copy of
all the entries are stored locally, encrypted by a passphrase.

## How to enable offline mode?

1) Click on the username at the top right

![Click on username at the top right](/images/user/offline_mode/Offline_Mode_enable.png)

2) Enter a passphrase

The client will download all the data from the server, encrypt it with the passphrase and store it locally on the device.

![Enter a passphrase](/images/user/offline_mode/Offline_Mode_passphrase.png)

## How to unlock offline storage

1) Unlock offline storage

When you open the Psono webinterface you will be automatically asked to enter your passphrase. If you lost your passphrase
you can logout and wipe the storage, allowing you to login again with your username and master password.

![Enter a passphrase](/images/user/offline_mode/Offline_Mode_unlock.png)

## How to disable the offline mode

1) Disable offline mode

To disable the offline mode you can click on the username at the top right, and select "Go Online".

![Disable offline mode](/images/user/offline_mode/Offline_Mode_disable.png)