---
title: Share file repositories
metaTitle: Share file repositories | Psono Documentation
meta:
  - name: description
    content: Instructions how to share file repositories with other users.
---

# Share file repositories

Instructions how to share file repositories with other users.

## Pre-requirements

We assume that you already have a file repository configured.

## Sharing a file repository

1) Login to Psono

![Step 1 Login](/images/user/basic/step1-login.jpg)

2) Go to "Other"

![Step 2 Go to other](/images/user/file_repository_share_repositories/step2-go-to-other.jpg)

3) Go to "File Repositories"

![Step 3 Go to "File Repositories"](/images/user/file_repository_share_repositories/step3-go-to-file-repositories.jpg)

4) Click the Edit / wrench symbol

Click the edit / wrench symbol for the file repository that you want to share

![Step 4 Click wrench](/images/user/file_repository_share_repositories/step4-click-wrench.jpg)

5) Go to "Access rights" and click "Add right"

![Step 5 go to "Access rights" and then click "Add right"](/images/user/file_repository_share_repositories/step5-go-to-access-rights-and-click-add-right.jpg)

6) Select the correct user

![Step 6 Select the correct user](/images/user/file_repository_share_repositories/step6-pick-the-user.jpg)

::: tip
If the user is not in the list, you can search him through 'Search User'.
:::

7) (optional) Adjust permissions

Now the user can only upload to this repository. If you want to give the user more capabilities you can select now
the appropriate rights for him.

![Step 7 Adjust permissions](/images/user/file_repository_share_repositories/step7-adjust-permissions.jpg)

Well done, your file repository is now shared with the other user. As a next step the other user has to accept the shared
repository.


## Accepting a shared file repository

1) Login to Psono

![Step 1 Login](/images/user/basic/step1-login.jpg)

2) Go to "Other"

![Step 2 Go to other](/images/user/file_repository_share_repositories/step2-go-to-other2.jpg)

3) Go to "File Repositories" and click "Accept"

![Step 3 Go to "File Repositories"](/images/user/file_repository_share_repositories/step3-click-accept.jpg)



Well done, you can now upload to this file repository.

