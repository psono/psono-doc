---
title: Setup other S3 compatible storage as file repository
metaTitle: Setup other S3 compatible storage as file repository | Psono Documentation
meta:
  - name: description
    content: Instructions how to setup another S3 compatible storage as file repository
---

# Setup other S3 compatible storage as file repository

Instructions how to setup another S3 compatible storage like minio as file repository.



## Pre-requirements

You need the connection settings like url, bucket, region, access key id and secret access key.


## Setup Guide

This guide will walk you through the general setup in the settings.yaml, before it helps you to configure it in psono.

### General settings

1) Whitelist file repository type

Whitelist other s3 storage as allowed file repository types by adding `other_s3` to `ALLOWED_FILE_REPOSITORY_TYPES` in the settings.yaml like:

```yaml
ALLOWED_FILE_REPOSITORY_TYPES: ['azure_blob', 'gcp_cloud_storage', 'aws_s3', 'do_spaces', 'backblaze', 'other_s3']
```

2) Restrict URLs

For security purposes you should restrict which URLs will be accessed / used with the file repository e.g.

```yaml
ALLOWED_OTHER_S3_ENDPOINT_URL_PREFIX: ['https://minio.example.com', 'https://otherminio.whatever.com']
```

Restart the server afterward. If you were already logged in, you need to logout and login again.




### Configure the file repository


1) Login to Psono

![Step 13 Login to Psono](/images/user/file_repository_other_s3_storage/step13-login-to-psono.jpg)

2) Go to "Other"

![Step 14 Go to other](/images/user/file_repository_other_s3_storage/step14-go-to-other.jpg)

3) Go to "File Repositories" and click "Create new file repository"

![Step 15 Go to "File Repositories" and click "Create new file repository"](/images/user/file_repository_other_s3_storage/step15-select-file-repository-and-click-create-new-file-repository.jpg)

4) Configure the file repository

Enter a title, select "Other S3 compatible storage" as type and enter the other details for your s3 storage configuration.

![Step 16 Configure the file repository](/images/user/file_repository_other_s3_storage/step16-configure-repository.png)

You can now upload files from the datastore to this file repository.





