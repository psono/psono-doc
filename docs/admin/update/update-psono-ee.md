---
title: 1b. Update Psono EE
metaTitle: Update Psono EE | Psono Documentation
meta:
  - name: description
    content: Update instruction for the Enterprise Edition of Psono
---

# Update Psono EE

Update instruction for the Enterprise Edition of the Psono

## Preamble

From time to time you want to update to the latest version. At this point we assume that you have created a backup of
your postgres database. If you have a possibility to snapshot your machine, please do so. Check that you are running
Postgres 13 or later.

## Update

1. Update the docker image

    ```bash
    docker pull psono/psono-combo-enterprise:latest
    ```

2. Stop old psono-combo-enterprise

    ```bash
    docker stop psono-combo-enterprise
    ```

3. Update the database

    ```bash
    docker run --rm \
      -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-combo-enterprise:latest python3 ./psono/manage.py migrate
   ```

4. Start new psono-combo-enterprise

    ```bash
    docker run --name psono-combo-enterprise-new \
        --sysctl net.core.somaxconn=65535 \
        -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/portal/config.json \
        -v /path/to/log/folder:/var/log/psono \
        -d --restart=unless-stopped -p 10200:80 psono/psono-combo-enterprise:latest
    ```

    ::: tip
    If you used custom volume mounts e.g. for custom branding or a custom CA certificate for LDAPs, then make sure to adjust the command accordingly and add your volume mounts.
    :::

5. Cleanup

    If everything works you can cleanup your containers with the following commands:

    ```bash
    docker rm psono-combo-enterprise
    docker rename psono-combo-enterprise-new psono-combo-enterprise
    ```

If anything fails you should be able to restore the database, and start the old docker container again.



