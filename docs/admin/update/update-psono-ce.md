---
title: 1a. Update Psono CE
metaTitle: Update Psono CE | Psono Documentation
meta:
  - name: description
    content: Update instruction for the Community Edition of the Psono
---

# Update Psono CE

Update instruction for the Community Edition of the Psono

## Preamble

From time to time you want to update to the latest version. At this point we assume that you have created a backup of
your postgres database. If you have a possibility to snapshot your machine, please do so. Check that you are running
Postgres 13 or later.

## Update

1. Update the docker image

    ```bash
    docker pull psono/psono-combo:latest
    ```

2. Stop old psono-combo

    ```bash
    docker stop psono-combo
    ```
3. Update the database

    ```bash
    docker run --rm \
      -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-combo:latest python3 ./psono/manage.py migrate
    ```

5. Start new psono-combo

    ```bash
    docker run --name psono-combo-new \
        --sysctl net.core.somaxconn=65535 \
        -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/portal/config.json \
        -d --restart=unless-stopped -p 10200:80 psono/psono-combo:latest
    ```

    ::: tip
    If you used custom volume mounts e.g. for custom branding, then make sure to adjust the command accordingly and add your volume mounts.
    :::

6. Cleanup

    If everything works you can cleanup your containers with the following commands:

    ```bash
    docker rm psono-combo
    docker rename psono-combo-new psono-combo
    ```

If anything fails you should be able to restore the database, and start the old docker container again.
