---
title: SAML - Univention
metaTitle: Univention as SAML IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of Univention as SAML IDP
---

# Univention as SAML IDP for SSO

## Preamble

The EE server and client support the SAML protocol that allows you to configure an external service as IDP (identity provider)
for SSO (single sign on).
This guide here will explain how to configure Univention as SAML IDP for SSO.
We assume that your webclient is running on https://example.com, the server is reachable with
`https://example.com/server` (e.g. `https://example.com/server/info/` shows you some nice json output). This is your first
SAML provider that you want to configure (therefore we give him the ID "1").


::: tip
This feature is only available in the Enterprise Edition.
:::

## Univention

As a first step we have to configure Univention.

1.  Go to Domain / SAML Identity Provider

    ![Go to Domain / SAML identity provider](/images/admin/configuration/saml_univention_go_to_domain_saml.png)

2.  Click "Add"

    ![Click "Add"](/images/admin/configuration/saml_univention_add.png)

3.  Fill out the required fields and activate the service provider

    ![Fill out the required fields and activate the service provider"](/images/admin/configuration/saml_univention_fill_out_fields.png)

4.  Switch to Extended Settings and fill out the LDAP mapping information

    ![Switch to Extended Settings and fill out the LDAP mapping information](/images/admin/configuration/saml_univention_ldap_mapping.png)


5.  Go back to the Univention Portal and select Users / Groups

    ![Go to Users / Groups"](/images/admin/configuration/saml_univention_go_to_users_groups.png)


6.  Select the group you want and add the Psono service provider under SAML settings

    ![SAML group settings](/images/admin/configuration/saml_univention_group_saml_settings.png)


## Server (settings.yaml)

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Generate SP certificate

    You will need a certificate for your service provider (SP) later. You can generate one easily with:

    ```bash
    openssl req -new -newkey rsa:2048 -x509 -days 3650 -nodes -sha256 -out sp_x509cert.crt -keyout sp_private_key.key
    ```

    This will generate a private key (sp_private_key.key) and the public certificate (sp_x509cert.crt).

2.  Comment in the following section:

	```yaml
		SAML_CONFIGURATIONS:
		1:
			idp:
				entityId: "UNIVENTION_SSO_SAML_IDENTITY_PROVIDER_ISSUE"
				singleLogoutService:
					binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
					url: "UNIVENTION_SSO_SIGN_OUT_URL"
				singleSignOnService:
					binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
					url: "UNIVENTION_SSO_SIGN_IN_URL"
				x509cert: "UNIVENTION_SSO_X509_CERTIFICATE"
				groups_attribute: "groups"
				username_attribute: 'username'
				email_attribute: 'email'
				username_domain: 'example.com'
				required_group: []
				is_adfs: false
				honor_multifactors: true
				max_session_lifetime: 86400
			sp:
				NameIDFormat: "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"
				assertionConsumerService:
					binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"
				attributeConsumingService:
					serviceName: "SP_SERVICE_NAME"
					serviceDescription: "Psono password manager"
					requestedAttributes:
						-
							attributeValue: []
							friendlyName: ""
							isRequired: false
							name: "attribute-that-has-to-be-requested-explicitely"
							nameFormat: ""
				privateKey: "SP_PRIVATE_CERTIFICATE"
				singleLogoutService:
					binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
				x509cert: "SP_X509CERT"
				autoprovision_psono_folder: false
				autoprovision_psono_group: false
			strict: true
	```

	- Replace `UNIVENTION_SSO_SAML_IDENTITY_PROVIDER_ISSUE` with: https://univention.example.com/simplesamlphp/saml2/idp/metadata.php
	- Replace `UNIVENTION_SSO_SIGN_IN_URL` with the Sign-In URL from Univention: https://univention.example.com/simplesamlphp/saml2/idp/SSOService.php
	- Replace `UNIVENTION_SSO_SIGN_OUT_URL` with the Sign-Out URL from Univention: https://univention.example.com/simplesamlphp/saml2/idp/SingleLogoutService.php
	- Replace `UNIVENTION_SSO_X509_CERTIFICATE` with the "X.509 Certificate" from Univention: https://univention.example.com/simplesamlphp/saml2/idp/certificate (remove all line breaks)
	- Replace `SP_SERVICE_NAME` with the "metadata" URL of your service provider, e.g. https://example.com/server/saml/1/metadata/
	- Replace `SP_PRIVATE_CERTIFICATE` with the content of the previous generated "sp_private_key.key". (remove all line breaks)
	- Replace `SP_X509CERT` with the content of the previous generated "sp_x509cert.crt". (remove all line breaks)

    Restart the server afterward

3.  Adjust authentication methods

    Make sure that `SAML` is part of the `AUTHENTICATION_METHODS` parameter in your settings.yaml e.g.

	```yaml
	AUTHENTICATION_METHODS: ['SAML']
	```
    Restart the server afterward

4.  (optional) Server Secrets

    By default the server will keep a copy of the user's secret keys to allow people to login without a password.
    If you want true client side encryption and as such force users to enter separate password for the encryption you specify
    the following in your settings.yaml. You can also decide later and change that and migrate users during the login
    or apply this setting only to particular users or groups with policies in the Admin Portal.

    ```yaml
    COMPLIANCE_SERVER_SECRETS: 'noone'
    ```

    ::: warning
    If a user loses his password he will lose all his data.
    :::


5.  (optional) Debug Mode

    It is helpful in the later debugging to enable debug mode.

	```yaml
	DEBUG: True
	```

    ::: warning
    Restart the server afterward and don't forget to remove it before going to production.
    :::

## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.


1. Basic
    
    Update your config.json similar to the one shown below.
    
    ```json
    {
      ...
        "authentication_methods": ["SAML"],
        "saml_provider": [{
          "title": "SAML Login",
          "provider_id": 1,
          "button_name": "Login "
        }]
      ...
    }
    ```
    
    The variable authentication_methods restricts the allowed login methods. In the example above only SAML will be allowed
    and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
    to match the one that you used on your server.

2. (optional) Automatic login
    
    You may want to "automatically" click on the login button to initiate the login flow. You can accomplish this by modifying the config.json as shown below:
    
    ```json
    {
      ...
        "authentication_methods": ["SAML"],
        "auto_login": true,
      ...
    }
    ```
    
    ::: warning
    This will only work if you have just one provider configured with only one authentication method. Users won't be able to modify
    the server url nor choose to register or interact with the login form in any other way.
    :::

