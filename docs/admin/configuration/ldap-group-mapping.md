---
title: LDAP Group Mapping
metaTitle: LDAP Group Mapping | Psono Documentation
meta:
  - name: description
    content: Configuration of the group mapping of LDAP groups to Psono groups
---

# LDAP Group Mapping

## Preamble

The EE server supports the LDAP protocol that allows you to configure an external LDAP service for authentication.
In addition the LDAP server may provide groups. This guide here will explain how to map a LDAP group to a Psono group.
We assume that you already have configured LDAP correctly with the necessary attribute configuration, so groups are
transferred proper. If not please check out the appropriate configuration guide.

::: tip
This feature is only available in the Enterprise Edition.
:::

## Admin Webclient

1.  Login to the Admin webclient

    ![Login Admin Webclient](/images/admin/configuration/ldap_group_mapping_login_portal.jpg)

2.  Sync LDAP Groups

    Go to `LDAP` -> `Groups` and click the `SYNC WITH LDAP` button

    ![Sync LDAP Groups](/images/admin/configuration/ldap_group_mapping_sync_ldap_groups.jpg)

3.  Create Managed Group

    Go to `Groups` and click the `+` button to create a managed group

    ![Create Managed Group](/images/admin/configuration/group_mapping_create_managed_group.png)

4.  Edit Managed Group

    Go to `Groups` and click the `pencil` button next to the created group

    ![Edit Managed Group](/images/admin/configuration/group_mapping_edit_managed_group.png)

5.  Create Mapping

    Search for the LDAP group and click the checkmark symbol in the "Mapped" column.

    ![Create Mapping](/images/admin/configuration/ldap_group_mapping_create_mapping.jpg)

6.  (optional) Grant Share Admin

    If you want to allow users of this LDAP group to add new shares to this group, you have to grant them Share Admin.

    ![Grant Share Admin](/images/admin/configuration/ldap_group_mapping_grant_share_admin.jpg)

    ::: tip
    It is considered best practise to share only one folder per group and add new entries or subfoldes to that one shared folder. That way all shares are instant and noone has to accept new shared secrets.
    :::

7.  Finished

    Whenever a user logs in with LDAP, the server will map Psono groups according to the user's LDAP groups and grant
    the user the necessary permissions. If a user loses access to a group the server will remove the user from the Psono
    group upon next login.

