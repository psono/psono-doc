---
title: Policies
metaTitle: Policies for users and groups | Psono Documentation
meta:
  - name: description
    content: Configuration of policies for users and groups
---

# Policies

## Preamble

The EE server supports policies, that can be used to specify certain restrictions on a user or group level. 
These can be used to overwrite certain compliance settings so that individual users or groups have less restrictions.
We assume that your webclient is running on https://example.com, the portal is reachable with
`https://example.com/portal/`

::: tip
This feature is only available in the Enterprise Edition.
:::

## Create Policy

1.  In the portal go to Policies

    ![Go to policies](/images/admin/configuration/policies_go_to_policies.png)

2.  Click on the plus to create a new policy

    ![Go to policies](/images/admin/configuration/policies_click_plus.png)

3.  Enter a title for your policy and click "Create Policy"

    ![Go to policies](/images/admin/configuration/policies_enter_title.png)

4.  Adjust Priority

    Once created, the system should directly open the new policy so you can fine-tune the settings.
    Adjust the priority as necessary. Settings specified in policies with higher priorities will override those with lower priorities.

    ![Go to policies](/images/admin/configuration/policies_specify_priority.png)

5.  Configure settings

    Under the `Settings` tab, chose the parameters that you'd like to overwrite and configure the value accordingly.

    ![Go to policies](/images/admin/configuration/policies_configure_settings.png)

6.  Configure users

    You can now apply this policy to users and/or groups. To apply this policy to users, choose the `Users` tab,
    use the search function to find the user, and then check the checkbox in the `Mapped` column.

    ![Go to policies](/images/admin/configuration/policies_apply_to_users.png)

6.  Configure groups

    To apply this policy to groups, choose the `Groups` tab,
    use the search function to find the group, and then check the checkbox in the `Mapped` column.

    ![Go to policies](/images/admin/configuration/policies_apply_to_groups.png)

    ::: warning
    Users in general are allowed to leave groups. As such policies should eather only grant permissions, so that a user cannot
    bypass restrictions by leaving a group. An alternative approach would be to configure "Forced Membership" on the group, which will
    prevent that a user can leave the group.
    :::

7.  (Optional) Configure Forced Membership

    Go to `Users` -> `Groups` and click the `pencil` button next to the group to edit the group. Afterwards check `Forced Membership` and click on Save.
    That way a user cannot leave the group nor deny the request to join the group. Policies for these groups are also applied before a user accepts the group membership.

    ![Forced group membership](/images/admin/configuration/policies_forced_groups.png)

