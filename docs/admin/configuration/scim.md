---
title: SCIM
metaTitle: SCIM user and group management | Psono Documentation
meta:
  - name: description
    content: Configuration of SCIM user and group provisioning
---

# SCIM (Beta)

## Preamble

The EE server support user and group management with SCIM, in combination with SAML. SCIM allows external services
to create / delete / update users and groups. Psono doesn't implement the full SCIM specification and instead currently supports
the subset required by Azure.
This guide here will explain how to configure SCIM. We assume that your webclient is running on https://example.com, the server is reachable with
`https://example.com/server` (e.g. `https://example.com/server/info/` shows you some nice json output).

::: tip
This feature is only available in the Enterprise Edition.
:::

## Enable SCIM

Lookup the provider id of your SAML configuration. If your configuration looks like this:

```yml
SAML_CONFIGURATIONS:
    1:
        idp:
            entityId: ...
            ...
```

Then your provider id is 1. So create a SCIM configuration that looks like this:

```yml
SCIM_CONFIGURATIONS:
    1:
        TOKEN: 'Replace me with a secure long random string'
        AUTHENTICATION_METHOD: 'SAML'
        PROVIDER_ID: 1
        AUTOPROVISION_PSONO_GROUP: False
        FORCE_MEMBERSHIP_OF_AUTOPROVISIONED_GROUPS: False
        AUTOPROVISION_PSONO_FOLDER: False
```

- Change `TOKEN` parameter and replace it with a secure random string. This will later be used by the SCIM provider as authentication.
- Change `PROVIDER_ID` parameter to match your SAML configuration's provider id
- Change `AUTOPROVISION_PSONO_GROUP` parameter and set it to true, if you want that Psono automatically creates a corresponding Psono group for any new SCIM group and an appropriate group mapping.
- Change `AUTOPROVISION_PSONO_FOLDER` parameter and set it to true, if you want that Psono automatically creates a folder with the same name as the group and shares it with the automatically created Psono Group. (Requires `AUTOPROVISION_PSONO_GROUP` to be set to true to have any effect.)
- Change `FORCE_MEMBERSHIP_OF_AUTOPROVISIONED_GROUPS` parameter and set it to true, if you want that Psono automatically checks "forced membership" for every automatically created Psono Group. (Requires `AUTOPROVISION_PSONO_GROUP` to be set to true to have any effect.)

Restart the server afterward.

The SCIM endpoint is now: `https://example.com/server/scim/2.0/1`, so e.g.

- `https://example.com/server/scim/2.0/1/Schema`
- `https://example.com/server/scim/2.0/1/Users`
- `https://example.com/server/scim/2.0/1/Groups`

Users and groups should now automatically be provisioned.