---
title: Audit Log with Splunk
metaTitle: Audit Log with Splunk | Psono Documentation
meta:
  - name: description
    content: Configuration of Audit Logging with Splunk
---

# Audit Log with Splunk

## Preamble

This guide explains how to use Splunk with Psono's audit logging. We assume that you have followed this general
[guide for audit log](/admin/configuration/audit-log.html) to configure the audit logs and see now events in your
`audit.log` file.

::: tip
This feature is only available in the Enterprise Edition.
:::

## Shipping Logs

To ship your logs you have various highly specialized options that all depend on your infrastructure. We only focus here
on the most prominent one with `Splunk Universal Forwarder` and Psono's custom solution with the help of `Splunk's HTTP Event Collector`

### Splunk Universal Forwarder

The most prominent option is to install a `Splunk Universal Forwarder`, that watches the `audit.log` file.

1.  Installation

    Instructions how to install one in general can be found here [docs.splunk.com/Documentation/Forwarder/8.1.0/Forwarder/Installanixuniversalforwarder](https://docs.splunk.com/Documentation/Forwarder/8.1.0/Forwarder/Installanixuniversalforwarder)

1.  Install Add-On

    Afterwards you can install the [Splunk Add-On for Psono](https://splunkbase.splunk.com/app/4456/). It will contain monitors for the `audit.log` and health checks.


### Native

::: warning
Please be aware that Psono's native Splunk implementation is not as sophisticated as a `Splunk Universal Forwarder` and will lose data in the event of e.g. network issues.
:::

1.  Configure Splunk HTTP Event Collector

    Psono's native implementation relies on the `Splunk HTTP Event Collector`. Detailed information how to configure one
    can be found here [dev.splunk.com/enterprise/docs/devtools/httpeventcollector/](https://dev.splunk.com/enterprise/docs/devtools/httpeventcollector/)
    
    Please take notes of the host, port and the token.

1.  Configure Psono server

    There are a couple of variables that you need / can adjust in the `settings.yaml`
    
    - `SPLUNK_HOST` The host, e.g. an ip or a domain
    - `SPLUNK_PORT` The port, e.g. 8088 that you configured in the splunk http event collector
    - `SPLUNK_TOKEN` The token of your splunk http event collector
    - `SPLUNK_INDEX` The splunk index that you want the events to end up in. By default 'main'
    - `SPLUNK_PROTOCOL` 'http' or 'https' to indicate the protocol. By default 'https'
    - `SPLUNK_VERIFY` True or False to indicate whether to verify certificates. By default True
    - `SPLUNK_SOURCETYPE` The source type. By default 'psono:auditLog' (that one is compatible with the provided splunk addons)
    
    ::: tip
    Don't forget to restart the server afterward.
    :::
    
    ::: tip
    More infos can be found here [github.com/zach-taylor/splunk_handler](https://github.com/zach-taylor/splunk_handler)
    :::


## Visualising Logs

We are providing here an app that contains dashboards that will help you to understand and audit all events that are recorded.

[Splunk App for Psono](https://splunkbase.splunk.com/app/4455/)

In order for the dashboards to work the logs need to have the correct source type of `psono:auditLog` (the default for `SPLUNK_SOURCETYPE`).