---
title: LDAP Extras
metaTitle: LDAP Extras | Psono Documentation
meta:
  - name: description
    content: Additional configuration details for LDAP
---

# LDAP Extras

## Preamble

The EE server supports the LDAP protocol that allows you to configure an external LDAP service for authentication.
You may configure certain extras, e.g. to restrict access to members of a certain group. This guide will explain how
to do that.

::: tip
This feature is only available in the Enterprise Edition.
:::

## Restrict to members of certain LDAP group

You can configure the server to allow only members of a certain group to access and use Psono.

```yaml
LDAP : [
  {
    ...
    'LDAP_REQUIRED_GROUP': ['CN=g1,OU=Groups,OU=example.com,DC=example,DC=com']
    ...
  },
]
```

Restart the server afterward.

::: tip
The filtering is done by Python and as such is case sensitive!
:::

## Restrict to users with a certain attribute

You can configure the server to allow only LDAP users with a certain attribute to access and use Psono. You can either
force the presence of the attribute (use `*` to require only the presence) or require it to be set to a certain value.

```yaml
LDAP : [
  {
    ...
    'LDAP_REQUIRED_ATTRIBUTE': [['displayName', '*']]
    ...
  },
]
```

Restart the server afterward.

::: tip
The filtering is done by Python and as such is case sensitive!
:::

## Filter users with custom LDAP query

If the standard options are not enough to filter users, you can also provide some custom LDAP filtering on top. So e.g.
to filter users by the absence of a certain attribute (here `nsaccountlock`) you could do the following:

```yaml
LDAP : [
  {
    ...
    'LDAP_CUSTOM_USER_FILTER'='(!(nsaccountlock=TRUE))'
    ...
  },
]
```

## Filter groups with custom LDAP query

If the standard options are not enough to filter groups, you can also provide some custom LDAP filtering on top. So e.g.
to filter groups by the absence of a certain attribute (here `nsaccountlock`) you could do the following:

```yaml
LDAP : [
  {
    ...
    'LDAP_CUSTOM_GROUP_FILTER'='(!(nsaccountlock=TRUE))'
    ...
  },
]
```

Restart the server afterward.

## Automatically create Psono groups

You may want to automatically create a Psono Group for all created / imported LDAP groups and a corresponding mapping.

```yaml
LDAP : [
  {
    ...
    'AUTOPROVISION_PSONO_GROUP': True
    ...
  },
]
```

Restart the server afterward.

## Set forced membership for created groups

If you automatically create Psono groups for every LDAP group with the `AUTOPROVISION_PSONO_GROUP` flag, you can set
this flag here so that every new group has "forced membership" checked.

```yaml
LDAP : [
  {
    ...
    'FORCE_MEMBERSHIP_OF_AUTOPROVISIONED_GROUPS': True
    ...
  },
]
```

Restart the server afterward.

## Automatically create shared folder for group

You may want to automatically create a new folder and share it automatically with any new created / imported LDAP group. Requires `AUTOPROVISION_PSONO_GROUP` to be set to true to have any effect.

```yaml
LDAP : [
  {
    ...
    'AUTOPROVISION_PSONO_FOLDER': True
    ...
  },
]
```

Restart the server afterward.

## Prevent paging

Some servers might not allow LDAP paging. You can disable this 

```yaml
LDAP : [
  {
    ...
    'PAGING_ENABLED': False
    ...
  },
]
```

Restart the server afterward.

## Search users by LDAP attributes

You may want to allow users to search others e.g. by their first name or lastname which may not part of the created "username" attribute.

In the example below we assume that you have two ldapAttributes e.g. "firstName" and "lastName" containing the first and lastname of a user.
With the config below these attributes would be imported into Psono, and allow people to search for those attributes.
This works only in combination with `ALLOW_USER_SEARCH_BY_USERNAME_PARTIAL` being set to True.

```yaml
ALLOW_USER_SEARCH_BY_USERNAME_PARTIAL: True

LDAP : [
  {
    ...
    'LDAP_ATTR_DISPLAY_NAME': ['firstName', 'lastName']
    ...
  },
]
```

Restart the server afterward. User's may have to logout and login to trigger the LDAP sync and for the client to load these settings.