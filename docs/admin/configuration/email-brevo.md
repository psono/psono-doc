---
title: Email - Brevo
metaTitle: Email configuration with Brevo | Psono Documentation
meta:
  - name: description
    content: Configuration of email delivery with Brevo
---

# Email configuration with Brevo

## Preamble

The server supports multiple email providers. This guide will explain how to configure the Psono server to use Brevo for
email delivery.

## Configuration

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Configure email address

	```yaml
	EMAIL_FROM: 'something@example.com'
	```

	All emails that are sent by the server will come from this email address.

    Restart the server afterward

2.  Add Brevo API credentials to setting.yml

	```yaml
	EMAIL_BACKEND: 'anymail.backends.brevo.EmailBackend'
    BREVO_API_KEY: 'YOUR_BREVO_API_KEY'
	```

    Replace `YOUR_BREVO_API_KEY` with the API key that was provided to you by Brevo.

    Restart the server afterward


## Testing

To send a test email to `something@something.com` execute:

    docker run --rm \
      -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-combo:latest python3 ./psono/manage.py sendtestmail something@something.com

If you receive this test email, then email should be configured proper.


## More Information

Psono is using Anymail under the hood. You can check out the official documentation here:

[anymail.readthedocs.io/en/stable/esps/brevo/](https://anymail.readthedocs.io/en/stable/esps/brevo/)
