---
title: OIDC Extras
metaTitle: OIDC Extras | Psono Documentation
meta:
  - name: description
    content: Additional configuration details for OIDC
---

# OIDC Extras

## Preamble

The EE server supports the OIDC protocol that allows you to configure an external OIDC service for authentication.
You may configure certain extras, e.g. modify the session duration. This guide will explain how
to do that.

::: tip
This feature is only available in the Enterprise Edition.
:::

## Limit maximum session duration

A client will by default always request a session for 24h. This parameter is in seconds (default 86400) and allows you to reduce that.

```yaml
OIDC_CONFIGURATIONS:
    1:
        ...
        OIDC_MAX_SESSION_LIFETIME: 86400
```

Restart the server afterward.

## Increase session duration

A client will by default always request a session for 24h. This parameter is in seconds (default 0)  and allows you to increase that.

```yaml
OIDC_CONFIGURATIONS:
    1:
        ...
        OIDC_MIN_SESSION_LIFETIME: 86400
```

Restart the server afterward.
