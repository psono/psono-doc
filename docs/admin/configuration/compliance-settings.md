---
title: Compliance Settings
metaTitle: Compliance Settings | Psono Documentation
meta:
  - name: description
    content: Guide to explain common compliance settings.
---

# Compliance Settings

## Preamble

This guide will explain the various compliance settings that Psono offers in order to fulfill various security and compliance standards (HIPAA, ISO27001, PCI-DSS, ...).

::: tip
All features starting with `COMPLIANCE_` are only available in the Enterprise Edition.
:::

## Settings

### Enforce central security reports

If you have the requirement to audit your user's passwords and check passwords for data breaches and so on, then this
setting comes in handy. Normally users can choose whether to upload the result of a security report to the server or not.
This setting will force all users to upload the results of the security report to the server so an admin
or auditor can analyze it.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_ENFORCE_CENTRAL_SECURITY_REPORTS: True # or default False
```
Restart the server afterward

### Enforce regular security reports

If you have the requirement that your user's audit their passwords regulary, then you can force them to do so in a
fix interval.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_CENTRAL_SECURITY_REPORT_SECURITY_RECURRENCE_INTERVAL: 2592000
```
(The value is in seconds, so 2592000 seconds = 30x24x3600 seconds = 30 days)
A value of 0 would disable it. Restart the server afterward

### Enforce second factor

If you have the requirement to enforce a second factor for all users, you can do so with this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_ENFORCE_2FA: True # or default False
```
Restart the server afterward

### Disable exports

Maybe you don't want to allow your users to export their passwords. Then you can enforce this restriction with this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_EXPORT: True # or default False
```
Restart the server afterward

### Disable export of shared items

Maybe you don't want to allow your users to export shared items and only their own ones. Then you can enforce this restriction with this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_EXPORT_OF_SHARED_ITEMS: True # or default False
```
Restart the server afterward

### Disable unmanaged groups

Maybe you don't want to allow your users to create their own groups. Then you can disable this feature with with this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_UNMANAGED_GROUPS: True # or default False
```
Restart the server afterward

### Disable self deletion

Maybe you don't want to allow your users to delete their own account. You can achieve this with this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_DELETE_ACCOUNT: True # or default False
```
Restart the server afterward

### Disable api keys

Maybe you don't want to allow your users to bypass second factor restriction with API keys, then you can disable
them with this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_API_KEYS: True # or default False
```
Restart the server afterward

### Force or prevent server secrets

The server will by default keep for SAML / OIDC / LDAP users a backup of the user's keys and as such not ask the user for
encryption password during login, while the server won't know / store those keys for regular AUTHKEY users (`COMPLIANCE_SERVER_SECRETS: 'auto'`).
You may want to change this behavior and potentially force users to use a separate encryption password (`COMPLIANCE_SERVER_SECRETS: 'noone'`, most secure but no way to regain access to an account if a user loses his encryption password)
or force even AUTHKEY users to hand over their keys to the server (`COMPLIANCE_SERVER_SECRETS: 'all'`, least secure, but allows admins to set a new password for a user if he loses his password)


Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_SERVER_SECRETS: 'noone' # or 'all' or default 'auto'
```
Restart the server afterward

### Disable emergency codes

This setting allows you to disable emergency codes.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_EMERGENCY_CODES: True # or default False
```
Restart the server afterward

### Disable recovery codes

This setting allows you to disable recovery codes.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_RECOVERY_CODES: True # or default False
```
Restart the server afterward

### Disable file repositories

Maybe you have the requirement that all file exchange need to go through an antivirus scanner or just don't want
to provide your users the possibility to bypass internet proxies that are configured to prevent data exfiltration.
This setting allows you to disable file repositories.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_FILE_REPOSITORIES: True # or default False
```
Restart the server afterward

### Disable link shares

Link shares allow users to share content with "not registered" users, as such it's harder to audit access. This
setting allows you to disable link shares.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_LINK_SHARES: True # or default False
```
Restart the server afterward

### Disable offline mode

You may want to restrict users to create an "offline copy" and as such enter "offline mode". This setting allows you
to disable the offline mode.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_DISABLE_OFFLINE_MODE: True # or default False
```
Restart the server afterward

### Restrict maximum offline cache duration

You may want to restrict how long a user can use the offline cache without a server connection. This setting allows you
to configure this.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_MAX_OFFLINE_CACHE_TIME_VALID: 31536000
```
(The value is in seconds, so 31536000 seconds = 365x24x3600 seconds = 365 days)
Restart the server afterward.

::: tip
This feature currently only works for the mobile app.
:::

### Enforce minimum password length

One of the classics. This feature allows you to enforce a minimum password length.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_MIN_MASTER_PASSWORD_LENGTH: 14
```
Restart the server afterward


### Enforce minimum password complexity

Another classic. This feature allows you to enforce how many security groups the password needs to pass in order to
be rated complex enough. Psono security groups distinguish between lowercase, uppercase, numbers, special chars.
So a setting of 3 would require that a password matches at least 3 of the mentioned 4 security groups.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_MIN_MASTER_PASSWORD_COMPLEXITY: 3
```
Restart the server afterward


### IP Restrictions

This feature allows you to restrict the usage (webclient, apps, portal, API keys) of Psono to certain ip ranges.
Specifically excluded are link shares and all API endpoints for the fileserver, SCIM and remote management commands.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_IP_RESTRICTIONS: '10.5.0.0/24,10.4.0.0/16'
```
Restart the server afterward


### Clipboard clear delay

The clipboard is usually cleared automatically once a user copies something to the clipboard. This setting configures
the default in seconds. A value of 0 disables it.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_CLIPBOARD_CLEAR_DELAY: 30
```
Restart the server afterward


### Minimum clipboard clear delay

The clipboard is usually cleared automatically once a user copies something to the clipboard. This setting configures
the minimum a user can set in seconds.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_MIN_CLIPBOARD_CLEAR_DELAY: 0
```
Restart the server afterward


### Maximum clipboard clear delay

The clipboard is usually cleared automatically once a user copies something to the clipboard. This setting configures
the maximum a user can set in seconds.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_MAX_CLIPBOARD_CLEAR_DELAY: 600
```
Restart the server afterward


### Password generator default password length

You can provide a default configuration for the password generator. This setting configures the default length for newly
generated passwords. Please take note that a user can always overwrite this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_PASSWORD_LENGTH: 16
```
Restart the server afterward


### Password generator default uppercase letters

You can provide a default configuration for the password generator. This setting configures the default set of uppercase
letters for newly generated passwords. Please take note that a user can always overwrite this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_LETTERS_UPPERCASE: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
```
Restart the server afterward


### Password generator default lowercase letters

You can provide a default configuration for the password generator. This setting configures the default set of lowercase
letters for newly generated passwords. Please take note that a user can always overwrite this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_LETTERS_LOWERCASE: 'abcdefghijklmnopqrstuvwxyz'
```
Restart the server afterward


### Password generator default numbers

You can provide a default configuration for the password generator. This setting configures the default set of numbers
for newly generated passwords. Please take note that a user can always overwrite this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_NUMBERS: '0123456789'
```
Restart the server afterward


### Password generator default special characters

You can provide a default configuration for the password generator. This setting configures the default set of special
characters for newly generated passwords. Please take note that a user can always overwrite this setting.

Configure in your `settings.yml` the following line
```yaml
COMPLIANCE_PASSWORD_GENERATOR_DEFAULT_SPECIAL_CHARS: ',.-;:_#''+*~!"§$%&/()=?{[]}\\'
```
Restart the server afterward

::: warning
Make sure that you escape the characters properly. The YAML format maybe be "less intuitive" as you might think.
:::

### Logout after inactivity

Another classic. You can enforce an automatic logout after a certain time of inactivity. This setting can be used
to configured a timer that resets with every interaction and allows the session to be active for another X seconds.

Configure in your `settings.yml` the following line
```yaml
AUTO_PROLONGATION_TOKEN_TIME_VALID: 900
```
(The value is in seconds, so 900 seconds = 15x60 seconds = 15 minutes)
Restart the server afterward


### Multiple sessions

Maybe you need to prevent that a user is logged in with multiple devices. Then you can prevent multiple sessions
with the setting below, forcing a user to be logged out on other devices once he logs in.

Configure in your `settings.yml` the following line
```yaml
ALLOW_MULTIPLE_SESSIONS: False # or default True
```
Restart the server afterward

### Max session lifetime of webclient sessions

Allows you to restrict the time that a session from a webclient (and extensions) is valid.

Configure in your `settings.yml` the following line
```yaml
MAX_WEB_TOKEN_TIME_VALID: 2592000
```
(The value is in seconds, so 2592000 seconds = 30x24x3600 seconds = 30 days)
Restart the server afterward

::: tip
This feature will not work together with AUTO_PROLONGATION_TOKEN_TIME_VALID
:::


### Max session lifetime of app sessions

Allows you to restrict the time that a session from an app is valid.

Configure in your `settings.yml` the following line
```yaml
MAX_APP_TOKEN_TIME_VALID: 31536000
```
(The value is in seconds, so 31536000 seconds = 365x24x3600 seconds = 365 days)
Restart the server afterward

::: tip
This feature will not work together with AUTO_PROLONGATION_TOKEN_TIME_VALID
:::


### Max session lifetime of api key sessions

Allows you to restrict the time that a session from an api key is valid.

Configure in your `settings.yml` the following line
```yaml
MAX_API_KEY_TOKEN_TIME_VALID: 600
```
(The value is in seconds, so 600 seconds = 10x60 seconds = 10 minutes)
Restart the server afterward

::: tip
This feature will not work together with AUTO_PROLONGATION_TOKEN_TIME_VALID
:::


### Prevent old passwords to be reused

Allows you to restrict old passwords so they cannot be reused. This setting allows you to specify how many of their last
passwords should be blocked.

Configure in your `settings.yml` the following line
```yaml
DISABLE_LAST_PASSWORDS: 3
```
Restart the server afterward
