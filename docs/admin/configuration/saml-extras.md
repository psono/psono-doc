---
title: SAML Extras
metaTitle: SAML Extras | Psono Documentation
meta:
  - name: description
    content: Additional configuration details for SAML
---

# SAML Extras

## Preamble

The EE server supports the SAML protocol that allows you to configure an external SAML service for authentication.
You may configure certain extras, e.g. modify the session duration. This guide will explain how
to do that.

::: tip
This feature is only available in the Enterprise Edition.
:::

## Limit maximum session duration

A client will by default always request a session for 24h. This parameter is in seconds (default 86400)  and allows you to reduce that.


```yaml
SAML_CONFIGURATIONS:
    1:
        idp:
            ...
            max_session_lifetime: 43200
        sp:
            ...
```

Restart the server afterward.

## Increase session duration

A client will by default always request a session for 24h. This parameter is in seconds (default 0)  and allows you to increase that.

```yaml
SAML_CONFIGURATIONS:
    1:
        idp:
            ...
            min_session_lifetime: 0
        sp:
          ...
```

Restart the server afterward.
