---
title: Audit Log with S3
metaTitle: Audit Log with S3 | Psono Documentation
meta:
  - name: description
    content: Configuration of Audit Logging with S3
---

# Audit Log with S3

## Preamble

This guide explains how to instruct Psono to send audit logs directly to S3. We assume that you have followed this general
[guide for audit log](/admin/configuration/audit-log.html) to configure the audit logs and see now events in your
`audit.log` file.

::: tip
This feature is only available in the Enterprise Edition.
:::

## Shipping Logs
    
To ship your logs you need to create an S3 bucket, some credentials for that S3 bucket and configure then Psono to use these
credentials.

### Create bucket
    
1) Login to aws.amazon.com

2) Go to S3
    
    ![Step 3 Go to s3](/images/user/file_repository_setup_aws/01-go-to-s3.jpg)

3) Click "Create bucket"
    
    ![Step 3 Create bucket](/images/user/file_repository_setup_aws/02-click-create-bucket.jpg)

4) Specify bucket information and click "Create"
    
    ![Step 3 Specify bucket information](/images/user/file_repository_setup_aws/03-specify-bucket-information.jpg)
    
    ::: tip
    Remember the bucket name. You will need it later.
    :::


### Create a policy

1) Go to IAM
    
    ![Step 1 Go to IAM](/images/user/file_repository_setup_aws/31-go-to-iam.jpg)

2) Go to Policies and click "Create Policy"

    ![Step 2 Go to Policies and click Create Policy](/images/user/file_repository_setup_aws/21-go-to-pilicies.jpg)

3) Select JSON
    
    ![Step 3 Go to JSON](/images/user/file_repository_setup_aws/22-select-json-and-paste-content.jpg)
    
    and paste the following config:
    
    ```json
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "ListObjectsInBucket",
                "Effect": "Allow",
                "Action": [
                    "s3:ListBucket"
                ],
                "Resource": [
                    "arn:aws:s3:::psono-file-uploads"
                ]
            },
            {
                "Sid": "AllObjectActions",
                "Effect": "Allow",
                "Action": "s3:*Object",
                "Resource": [
                    "arn:aws:s3:::psono-file-uploads/*"
                ]
            }
        ]
    }
    ```


::: tip
Replace psono-file-uploads with your bucket name
:::

4) Click "Review Policy"

5) Specify a name and description

    ![Step 5 Specify a name and description](/images/user/file_repository_setup_aws/23-specify-name-and-description.jpg)

6) Click "Create Policy"


### Create a user

1) Go to IAM

    ![Step 1 Go to IAM](/images/user/file_repository_setup_aws/31-go-to-iam.jpg)

2) Go to users and click "Add User"
    
    ![Step 2 Go to users and click add user](/images/user/file_repository_setup_aws/32-click-add-user.jpg)

3) Specify a "name" and allow "programmatic access"

    ![Step 3 Specify some user information](/images/user/file_repository_setup_aws/33-specify-user-infos.jpg)

4) Attach your policy

    ![Step 4 Attach your policy](/images/user/file_repository_setup_aws/34-attach-policy.jpg)

5) Acquire "Access key ID" and "Secret access key"

    ![Step 5 Acquire "Access key ID" and "Secret access key"](/images/user/file_repository_setup_aws/35-get-secret-access-key.jpg)


### Configure Logging

1.  Configure Psono server

    There are a couple of variables that you need / can adjust in the `settings.yaml`
    
    - `S3_LOGGING_BUCKET` The bucket name
    - `S3_LOGGING_ACCESS_KEY_ID` The access key ID
    - `S3_LOGGING_SECRET_ACCESS_KEY` The secret access key
    
    ::: tip
    Don't forget to restart the server afterward.
    :::
