---
title: OIDC - Google Identity
metaTitle: Google Identity as OIDC IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of Google Identity as OIDC IDP
---

# Google Identity as IDP for OIDC-SSO

To set up the IDP you need access to the Google Cloud Console to create a new project and OAuth client credentials.

## Preamble

The Enterprise Edition (EE) server and client support the OIDC protocol that allows you to configure an external service
as IDP (identity provider) for SSO (single sign on). This guide here will explain how to configure Google Identity as OIDC-IDP for SSO. We assume that:

* your webclient can be accessed on https://psono.example.com
* the server is reachable at https://psono.example.com/server (e.g. https://psono.example.com/server/info/ shows you some nice json output). 

This is your first OIDC provider that you want to configure (therefore we give it the ID "1").

::: tip
This feature is only available in the Enterprise Edition.
:::

## Google Identity

1. Navigate to your [Google Cloud Console](https://console.cloud.google.com/)
2. Create a new project

    ![Create Google Cloud project](/images/admin/configuration/oidc_google_identity_new_project.png)

3. Navigate to APIs & Services -> Oauth Consent Screen
4. Configure OAuth consent screen and set it to "internal"

    ![Set OAuth consent screen to internal](/images/admin/configuration/oidc_google_identity_oauth_consent_screen.png)

5. Go to credentials
6. Create a new OAuth client ID

    ![Create new OAUTH client ID](/images/admin/configuration/oidc_google_identity_create_oauth_client_id_credentials.png)

7. Configure credentials
 
    Configure the credentials as "Web application" with `https://psono.example.com/server/oidc/<provider_id>/callback/`
    (adjust accordingly) as redirect URI
    ![Configure credentials](/images/admin/configuration/oidc_google_identity_configure_credentials.png)
    
    Note down the client id and client secret, as you will need them in the next step.
    
for more information check [Google's OpenID Connect documentation](https://developers.google.com/identity/protocols/oauth2/openid-connect#getcredentials )


## Server (settings.yaml)

After setting up the IDP for the OIDC-Authentication it is time to configure your running Psono server to act as the SP.
It is required that Psono can reach google and vise versa.


1.  Change or add OIDC configuration in to settings.yaml

    ```yml
    # https://accounts.google.com/.well-known/openid-configuration
    OIDC_CONFIGURATIONS:
        1:
            OIDC_RP_SIGN_ALGO: 'RS256'
            OIDC_RP_CLIENT_ID: 'xxxxxxxx.apps.googleusercontent.com'
            OIDC_RP_CLIENT_SECRET: 'xxxxxxx'
            OIDC_OP_JWKS_ENDPOINT: 'https://www.googleapis.com/oauth2/v3/certs'
            OIDC_OP_AUTHORIZATION_ENDPOINT: 'https://accounts.google.com/o/oauth2/v2/auth'
            OIDC_OP_TOKEN_ENDPOINT: 'https://oauth2.googleapis.com/token'
            OIDC_OP_USER_ENDPOINT: 'https://openidconnect.googleapis.com/v1/userinfo'
            OIDC_USERNAME_ATTRIBUTE: 'email'
            AUTOPROVISION_PSONO_GROUP: False
            FORCE_MEMBERSHIP_OF_AUTOPROVISIONED_GROUPS: False
            AUTOPROVISION_PSONO_FOLDER: False
    ```
    
    - The google endpoints can be found on the ".well-known"-page (e.g. https://accounts.google.com/.well-known/openid-configuration) of your installation
    
    ::: tip
    Always restart the server after making changes in the `setting.yml`-file.
    :::

2.  Adjust authentication methods

    Make sure that `OIDC` is part of the `AUTHENTICATION_METHODS` parameter in your settings.yaml e.g.

    ```yaml
    AUTHENTICATION_METHODS: ['OIDC']
    ```
    Restart the server afterward

3.  (optional) Server Secrets

    By default the server will keep a copy of the user's secret keys to allow people to login without a password.
    If you want true client side encryption and as such force users to enter separate password for the encryption you specify
    the following in your settings.yaml. You can also decide later and change that and migrate users during the login
    or apply this setting only to particular users or groups with policies in the Admin Portal.

    ```yaml
    COMPLIANCE_SERVER_SECRETS: 'noone'
    ```

    ::: warning
    If a user loses his password he will lose all his data.
    :::

## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.


1. Basic
    
    Update your config.json similar to the one shown below.
    
    ```json
    {
      ...
        "authentication_methods": ["OIDC"],
        "oidc_provider": [{
          "title": "OIDC Login",
          "provider_id": 1,
          "button_name": "Login "
        }]
      ...
    }
    ```
    
    The variable authentication_methods restricts the allowed login methods. In the example above only OIDC will be allowed
    and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
    to match the one that you used on your server.

2. (optional) Automatic login
    
    You may want to "automatically" click on the login button to initiate the login flow. You can accomplish this by modifying the config.json as shown below:
    
    ```json
    {
      ...
        "authentication_methods": ["OIDC"],
        "auto_login": true,
      ...
    }
    ```
    
    ::: warning
    This will only work if you have just one provider configured with only one authentication method. Users won't be able to modify
    the server url nor choose to register or interact with the login form in any other way.
    :::
