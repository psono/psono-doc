---
title: OIDC - Keycloak
metaTitle: Keycloak as OIDC IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of Keycloak as OIDC IDP
---

# Keycloak as IDP for OIDC-SSO

To set up the IDP you need a running instance of Keycloak with a configurable realm.

## Preamble

The Enterprise Edition (EE) server and client support the OIDC protocol that allows you to configure an external service
as IDP (identity provider) for SSO (single sign on). This guide here will explain how to configure KeyCloak as OIDC-IDP for SSO. We assume that:

* your Keycloak instance is running on https://keycloak.example.com,
* you have a configured a realm named "psono",
* your webclient can be accessed on https://psono.example.com
* the server is reachable at https://psono.example.com/server (e.g. https://psono.example.com/server/info/ shows you some nice json output). 

This is your first OIDC provider that you want to configure (therefore we give it the ID "1").

::: tip
This feature is only available in the Enterprise Edition.
:::

## Keycloak

1. Navigate to your realm and click on `Create client` in the `Clients`-Section.

   ![client-create](/images/admin/configuration/oidc_keycloak_screen00.png)

2. In there add your new client like shown below

    ![client-create](/images/admin/configuration/oidc_keycloak_screen01a.png)

    ![client-create](/images/admin/configuration/oidc_keycloak_screen01b.png)

    ![client-create](/images/admin/configuration/oidc_keycloak_screen01c.png)

3. Edit "Logout settings" 

   Afterwards click on the client to edit and scroll down to the bottom of the settings to the "Logout settings", uncheck "Front channel logout" and enter the "Backchannel logout URL": `https://psono.example.com/oidc/1/backchannel-logout/`

    ![client-config](/images/admin/configuration/oidc_keycloak_screen02.png)

4. Note down "Client secret"

   Switch to the "Credentials" tab and note the "Client secret" which will be used in the configuration of Psono later.

   ![client-config](/images/admin/configuration/oidc_keycloak_screen03.png)

5. Create group mapper

    Go to "Client scopes" and select the "Dedicated scope and mappers for this client" 
    
    ![keycloak click dedicated scope and mappers](/images/admin/configuration/oidc_keycloak_create_mapper.png)

   Click "Add predefined mappers"

   ![keycloak add predefined mappers](/images/admin/configuration/oidc_keycloak_create_mapper2.png)

   And add "groups"

   ![keycloak add groups mapper](/images/admin/configuration/oidc_keycloak_create_mapper3.png)
    

## Server (settings.yaml)

After setting up the IDP for the OIDC-Authentication it is time to configure your running Psono server to act as the SP.
It is required that Psono can reach Keycloak and vise versa.

1.  Change or add OIDC configuration in to settings.yaml

   ```yml
   OIDC_CONFIGURATIONS:
       1:
           OIDC_RP_SIGN_ALGO: 'RS256'
           OIDC_RP_CLIENT_ID: 'psono'
           OIDC_RP_CLIENT_SECRET: 'd61d048a-100e-4299-885f-b3e23539b60c'
           OIDC_OP_JWKS_ENDPOINT: 'https://keycloak.example.com/realms/psono/protocol/openid-connect/certs'
           OIDC_OP_AUTHORIZATION_ENDPOINT: 'https://keycloak.example.com/realms/psono/protocol/openid-connect/auth'
           OIDC_OP_TOKEN_ENDPOINT: 'https://keycloak.example.com/realms/psono/protocol/openid-connect/token'
           OIDC_OP_USER_ENDPOINT: 'https://keycloak.example.com/realms/psono/protocol/openid-connect/userinfo'
           OIDC_OP_END_SESSION_ENDPOINT: 'http://127.0.0.1:8080/realms/psono/protocol/openid-connect/logout'
           AUTOPROVISION_PSONO_GROUP: False
           FORCE_MEMBERSHIP_OF_AUTOPROVISIONED_GROUPS: False
           AUTOPROVISION_PSONO_FOLDER: False
   ```

   - OIDC_RP_CLIENT_ID: That's the Client ID in keycloak
   - OIDC_RP_CLIENT_SECRET: That's the Client secret in keycloak
   - OIDC_OP_..._ENDPOINT: The keycloak endpoints can be found on the ".well-known"-page (e.g. https://keycloak.example.com/realms/psono/.well-known/openid-configuration) of your installation.
   - AUTOPROVISION_PSONO_GROUP: Change `AUTOPROVISION_PSONO_GROUP` parameter and set it to true, if you want that Psono automatically creates a corresponding Psono group for any new OIDC group and an appropriate group mapping.
   - AUTOPROVISION_PSONO_FOLDER: Change `AUTOPROVISION_PSONO_FOLDER` parameter and set it to true, if you want that Psono automatically creates a folder with the same name as the group and shares it with the automatically created Psono Group. (Requires `AUTOPROVISION_PSONO_GROUP` to be set to true to have any effect.)
   - FORCE_MEMBERSHIP_OF_AUTOPROVISIONED_GROUPS: Change `FORCE_MEMBERSHIP_OF_AUTOPROVISIONED_GROUPS` parameter and set it to true, if you want that Psono automatically checks "forced membership" for every automatically created Psono Group. (Requires `AUTOPROVISION_PSONO_GROUP` to be set to true to have any effect.)

   ::: tip
   Always restart the server after making changes in the `setting.yml`-file.
   :::

2.  Adjust authentication methods

    Make sure that `OIDC` is part of the `AUTHENTICATION_METHODS` parameter in your settings.yaml e.g.

    ```yaml
    AUTHENTICATION_METHODS: ['OIDC']
    ```
    
    Restart the server afterward

3.  (optional) Server Secrets

    By default the server will keep a copy of the user's secret keys to allow people to login without a password.
    If you want true client side encryption and as such force users to enter separate password for the encryption you specify
    the following in your settings.yaml. You can also decide later and change that and migrate users during the login
    or apply this setting only to particular users or groups with policies in the Admin Portal.

    ```yaml
    COMPLIANCE_SERVER_SECRETS: 'noone'
    ```

    ::: warning
    If a user loses his password he will lose all his data.
    :::

## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.


1. Basic
   
   Update your config.json similar to the one shown below.
   
   ```json
   {
     ...
       "authentication_methods": ["OIDC"],
       "oidc_provider": [{
         "title": "OIDC Login",
         "provider_id": 1,
         "button_name": "Login "
       }]
     ...
   }
   ```
   
   The variable authentication_methods restricts the allowed login methods. In the example above only OIDC will be allowed
   and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
   to match the one that you used on your server.

2. (optional) Automatic login
   
   You may want to "automatically" click on the login button to initiate the login flow. You can accomplish this by modifying the config.json as shown below:
   
   ```json
   {
     ...
       "authentication_methods": ["OIDC"],
       "auto_login": true,
     ...
   }
   ```
   
   ::: warning
   This will only work if you have just one provider configured with only one authentication method. Users won't be able to modify
   the server url nor choose to register or interact with the login form in any other way.
   :::