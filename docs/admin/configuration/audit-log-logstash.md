---
title: Audit Log with Logstash
metaTitle: Audit Log with Logstash | Psono Documentation
meta:
  - name: description
    content: Configuration of Audit Logging with Logstash
---

# Audit Log with Logstash

## Preamble

This guide explains how to use Logstash with Psono's audit logging. We assume that you have followed this general
[guide for audit log](/admin/configuration/audit-log.html) to configure the audit logs and see now events in your
`audit.log` file.

::: tip
This feature is only available in the Enterprise Edition.
:::

## Shipping Logs

To ship your logs you have various highly specialized options that all depend on your infrastructure.

### Filebeat

The most prominent option is to install Filebeat, that watches the `audit.log` file and streams all entries.

1.  Installation

    Instructions how to install one in general can be found here [elastic.co/guide/en/beats/filebeat/current/index.html](https://www.elastic.co/guide/en/beats/filebeat/current/index.html)

### Native

1.  Configure Logstash

    Depending on your preference you can use one of the following configurations
    
    Example logstash.conf for unencrypted TCP transport:
    
        input {
            tcp {
                host => "127.0.0.1"
                port => 5959
                mode => server
                codec => json_lines {}
            }
        }
    
    Example logstash.conf for SSL-encrypted TCP transport:
    
        input {
            tcp {
                host => "127.0.0.1"
                port => 5958
                mode => server
                codec => json_lines {}
        
                ssl_enable => true
                ssl_verify => true
                ssl_extra_chain_certs => ["/etc/ssl/certs/logstash_ca.crt"]
                ssl_cert => "/etc/ssl/certs/logstash.crt"
                ssl_key => "/etc/ssl/private/logstash.key"
            }
        }
    
    Example logstash.conf for SSL-encrypted Beats transport:
    
        input {
            beats {
                host            => "127.0.0.1"
                port            => 5957
                ssl             => true
                ssl             => true
                ssl_verify_mode => "peer"
                ssl_certificate_authorities => ["/etc/ssl/certs/logstash_ca.crt"]
                ssl_certificate => "/etc/ssl/certs/logstash.crt"
                ssl_key         => "/etc/ssl/private/logstash.p8"
            }
        }
    
    Example logstash.conf for HTTP transport with basic authentication:
    
        input {
            http {
                host      => "127.0.0.1"
                port      => 5959
                user      => "logstash"
                password  => "testing"
                codec     => "json"
            }
        }

1.  Configure Psono server

    There are a couple of variables that you need / can adjust
    
    - `LOGSTASH_HANDLER` Shipping logs either async `logstash_async.handler.AsynchronousLogstashHandler` or in sync `logstash_async.handler.SynchronousLogstashHandler`. By default 'logstash_async.handler.SynchronousLogstashHandler'
    - `LOGSTASH_TRANSPORT` The transport to use. TCP: `logstash_async.transport.TcpTransport` or UDP: `logstash_async.transport.UdpTransport` or Beats `logstash_async.transport.BeatsTransport` or HTTP `logstash_async.transport.HttpTransport`. Defaults to 'logstash_async.transport.TcpTransport'
    - `LOGSTASH_HOST` The host, e.g. an ip or a domain
    - `LOGSTASH_PORT` The port, e.g. 5959 that you configured in Logstash. By default 5959
    - `LOGSTASH_SSL_ENABLED` Wether you want to use SSL or not. By default True
    - `LOGSTASH_SSL_VERIFY` True or False whether to verify certificates. By default True
    - `LOGSTASH_CA_CERTS` If you want a custom CA, you can specify here a path to the file with the certs
    - `LOGSTASH_CERFILE` The path to the cert file
    - `LOGSTASH_KEYFILE` The path to the key file
    
    ::: tip
    More infos can be found here [python-logstash-async.readthedocs.io/en/latest/index.html](https://python-logstash-async.readthedocs.io/en/latest/index.html)
    :::

