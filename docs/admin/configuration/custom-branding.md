---
title: Custom Branding
metaTitle: Custom Branding | Psono Documentation
meta:
  - name: description
    content: This guide will explain how to use custom branding.
---

# Custom Branding

## Preamble

This guide explains how to customize the look and feel of Psono. You can change logos, links and styles.

::: warning
Even so we try to keep paths, sizes, themes and so on all stable, it may happen from time to time that you have to
adjust your customizations to a new verison.
:::

## Client

### Top Logo

1.  Upload the logo

First upload the new logo to e.g. /opt/docker/psono-client/top_logo.png. The original logo has a resolution of 172x35 px.

2.  Change start command

	```bash
    docker run --name psono-combo \
        --sysctl net.core.somaxconn=65535 \
        -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/portal/config.json \
        -d --restart=unless-stopped -p 10200:80 psono/psono-combo:latest
	```

	Adjust this to include:

	```bash
    ...
        -v /opt/docker/psono-client/top_logo.png:/usr/share/nginx/html/img/logo-inverse.png \
    ...
	```
    Restart the client afterward

### Sign in Logo

1.  Upload the logo

    First upload the new logo to e.g. /opt/docker/psono-client/sign_in_logo.png. The original logo has a resolution of 172x35 px.

2.  Change start command

	```bash
    docker run --name psono-combo \
        --sysctl net.core.somaxconn=65535 \
        -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/portal/config.json \
        -d --restart=unless-stopped -p 10200:80 psono/psono-combo:latest
	```

	Adjust this to include:

	```bash
    ...
        -v /opt/docker/psono-client/sign_in_logo.png:/usr/share/nginx/html/img/logo.png \
    ...
	```
    Restart the client afterward

### Favicons

1.  Upload the favicons

    Psono is using favicons in multiple resolutions. Upload them to e.g. /opt/docker/psono-client. A list of the available
    files:
    
    - android-chrome-192x192.png
    - android-chrome-512x512.png
    - apple-touch-icon.png
    - appstore_apple.png
    - appstore_google.png
    - favicon-16x16.png
    - favicon-32x32.png
    - favicon.ico
    - icon-128.png
    - icon-16.png
    - icon-32.png
    - icon-48.png
    - icon-64.png
    - mstile-150x150.png

2.  Change start command

	```bash
    docker run --name psono-combo \
        --sysctl net.core.somaxconn=65535 \
        -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/portal/config.json \
        -d --restart=unless-stopped -p 10200:80 psono/psono-combo:latest
	```

	Adjust this to include your files e.g.:

	```bash
    ...
        -v /opt/docker/psono-client/android-chrome-192x192.png:/usr/share/nginx/html/img/android-chrome-192x192.png \
        -v /opt/docker/psono-client/android-chrome-512x512.png:/usr/share/nginx/html/img/android-chrome-512x512.png \
        -v /opt/docker/psono-client/apple-touch-icon.png:/usr/share/nginx/html/img/apple-touch-icon.png \
        -v /opt/docker/psono-client/appstore_apple.png:/usr/share/nginx/html/img/appstore_apple.png \
        -v /opt/docker/psono-client/appstore_google.png:/usr/share/nginx/html/img/appstore_google.png \
        -v /opt/docker/psono-client/favicon-16x16.png:/usr/share/nginx/html/img/favicon-16x16.png \
        -v /opt/docker/psono-client/favicon-32x32.png:/usr/share/nginx/html/img/favicon-32x32.png \
        -v /opt/docker/psono-client/favicon.ico:/usr/share/nginx/html/img/favicon.ico \
        -v /opt/docker/psono-client/icon-128.png:/usr/share/nginx/html/img/icon-128.png \
        -v /opt/docker/psono-client/icon-16.png:/usr/share/nginx/html/img/icon-16.png \
        -v /opt/docker/psono-client/icon-32.png:/usr/share/nginx/html/img/icon-32.png \
        -v /opt/docker/psono-client/icon-48.png:/usr/share/nginx/html/img/icon-48.png \
        -v /opt/docker/psono-client/icon-64.png:/usr/share/nginx/html/img/icon-64.png \
        -v /opt/docker/psono-client/mstile-150x150.png:/usr/share/nginx/html/img/mstile-150x150.png \
    ...
	```
    Restart the client afterward

### Custom Theme

1.  Create custom Theme

    Under the hood we are using Material UI 5. You can customize the theme including all colors, sizes and so. Create a 
    custom theme. You can take inspiration of our default theme below.:

    ```json
    {
        "palette": {
            "background": {
                "default": "#0f1118",
                "paper": "#fff"
            },
            "primary": {
                "main": "#2dbb93"
            },
            "secondary": {
                "main": "#0b4a23"
            },
            "action": {
                "disabledBackground": "#2dbb9380"
            },
            "lightGreyText": {
                "main": "#b1b6c1",
                "light": "#b1b6c1",
                "dark": "#b1b6c1",
                "contrastText": "#b1b6c1"
            },
            "greyText": {
                "main": "#666",
                "light": "#666",
                "dark": "#666",
                "contrastText": "#666"
            },
            "checked": {
                "main": "#9c27b0",
                "light": "#9c27b0",
                "dark": "#9c27b0",
                "contrastText": "#9c27b0"
            },
            "blueBackground": {
                "main": "#151f2b",
                "light": "#151f2b",
                "dark": "#151f2b",
                "contrastText": "#151f2b"
            },
            "badgeBackground": {
                "main": "#777",
                "light": "#777",
                "dark": "#777",
                "contrastText": "#777"
            },
            "appBarText": {
                "main": "#777",
                "light": "#777",
                "dark": "#777",
                "contrastText": "#777"
            },
            "appBarReadOnlyText": {
                "main": "#777",
                "light": "#777",
                "dark": "#777",
                "contrastText": "#777"
            },
            "appBarReadOnlyBackground": {
                "main": "#fad8a6",
                "light": "#fad8a6",
                "dark": "#fad8a6",
                "contrastText": "#fad8a6"
            },
            "appBarBackground": {
                "main": "#fff",
                "light": "#fff",
                "dark": "#fff",
                "contrastText": "#fff"
            },
            "baseBackground": {
                "main": "#ebeeef",
                "light": "#ebeeef",
                "dark": "#ebeeef",
                "contrastText": "#ebeeef"
            },
            "lightBackground": {
                "main": "#fff",
                "light": "#fff",
                "dark": "#fff",
                "contrastText": "#fff"
            },
            "baseTitleBackground": {
                "main": "#f2f5f7",
                "light": "#f2f5f7",
                "dark": "#f2f5f7",
                "contrastText": "#f2f5f7"
            }
        },
        "typography": {
            "fontFamily": "\"Open Sans\", sans-serif",
            "fontSize": 13
        },
        "components": {
            "MuiTextField": {
                "defaultProps": {
                    "margin": "dense",
                    "size": "small"
                }
            },
            "MuiToolbar": {
                "styleOverrides": {
                    "regular": {
                        "height": "48px",
                        "minHeight": "48px",
                        "@media(min-width:600px)": {
                            "minHeight": "48px"
                        }
                    }
                }
            },
            "MUIDataTable": {
                "styleOverrides": {
                    "paper": {
                        "boxShadow": "none"
                    }
                }
            },
            "MuiButton": {
                "styleOverrides": {
                    "containedPrimary": {
                        "color": "white"
                    },
                    "root": {
                        "color": "rgba(0, 0, 0, 0.87)"
                    }
                }
            }
        }
    }
    ```

2.  Add it to config.json

    ```json
	{
	  "backend_servers": [...],
	  "base_url": "https://psono.example.com/",
	  ...
	  "theme": {
        "palette": {
          ...
        },
        ...
      }
	}
    ```

    Restart the client afterward

### Custom CSS

1.  Create custom CSS file

    You should start off with one of our existing CSS files and modify it e.g. custom.css A list of all CSS files can be found here:
    https://gitlab.com/psono/psono-client/-/tree/master/src/common/data/css
    
    We assume that you have your modified copy present in e.g. /opt/docker/psono-client/custom.css

2.  Change start command

	```bash
    docker run --name psono-combo \
        --sysctl net.core.somaxconn=65535 \
        -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/portal/config.json \
        -d --restart=unless-stopped -p 10200:80 psono/psono-combo:latest
	```

	Adjust this to include:

	```bash
    ...
        -v /opt/docker/psono-client/custom.css:/usr/share/nginx/html/css/custom.css \
    ...
	```
    Restart the client afterward
    
### Custom "More" links

There are a couple of links shown in the left sidebar in the "more" section. You can customize them.

1.  Modify config.json

    Usually you start off with a config.json like this:
    
    ```json
    {
      "backend_servers": [...],
      "base_url": "https://psono.example.com/",
      ...
    }
    ```
        
    You can now modify it to show other more links like shown below:
    
	```json
	{
	  "backend_servers": [...],
	  "base_url": "https://psono.example.com/",
	  ...
	  "more_links": [{
          "href": "https://support.example.com/",
          "title": "Company Support"
      }]
	}
	```
        
    If you want to add the original links to then this is how it would looks like:
	```json
	{
	  "backend_servers": [...],
	  "base_url": "https://psono.example.com/",
	  ...
	  "more_links": [{
          "href": "https://doc.psono.com/",
          "title": "DOCUMENTATION",
          "class": "fa-book"
      },{
          "href": "privacy-policy.html",
          "title": "PRIVACY_POLICY",
          "class": "fa-user-secret"
      },{
          "href": "https://www.psono.com",
          "title": "ABOUT_US",
          "class": "fa-info-circle"
      }]
	}
	```
	
	Here you also see the usage of custom CSS classes to show custom icons. Psono currently supports Fontawesome 4.0 icons.
	
    Restart the client afterward

## Server

### Logo in E-Mails

1.  Upload the logo

    First upload the new logo to e.g. /opt/docker/psono-server/email_logo.png

2.  Change start command 

	```bash
    docker run --name psono-combo \
        --sysctl net.core.somaxconn=65535 \
        -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/portal/config.json \
        -d --restart=unless-stopped -p 10200:80 psono/psono-combo:latest
	```

	Adjust this to include:

	```bash
    ...
        -v /opt/docker/psono-server/email_logo.png:/root/psono/static/email/logo.png \
    ...
	```
    Restart the server afterward

### Email templates

1.  Download the existing

    First copy all the email templates from e.g. https://gitlab.com/psono/psono-server/-/tree/master/psono/templates/email?ref_type=heads
    into a folder, e.g. /opt/docker/email-templates

2.  Change start command 

    You can now replace one email template or the whole template folder with e.g.

	```bash
    docker run --name psono-combo \
        --sysctl net.core.somaxconn=65535 \
        -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/portal/config.json \
        -v /opt/docker/email-templates:/root/psono/templates/email \
        -d --restart=unless-stopped -p 10200:80 psono/psono-combo:latest
	```
 
    As you can see we added there `-v /opt/docker/email-templates:/root/psono/templates`.
    You could also do `-v /opt/docker/email-templates/new_share_created_subject.txt:/root/psono/templates/new_share_created_subject.txt` to shadow a single template instead.

    Restart the server afterward
