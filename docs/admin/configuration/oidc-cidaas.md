---
title: OIDC - cidaas
metaTitle: cidaas as OIDC IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of cidaas as OIDC IDP
---

# cidaas as IDP for OIDC-SSO

To set up the IDP you need a running instance of cidaas.

## Preamble

The Enterprise Edition (EE) server and client support the OIDC protocol that allows you to configure an external service
as IDP (identity provider) for SSO (single sign on). This guide here will explain how to configure [cidaas](https://www.cidaas.com/) as OIDC-IDP for SSO. We assume that:

* your cidaas instance is running on https://cidaas.test.de
* your webclient can be accessed on https://psono.test.de
* the server is reachable at https://psono.test.de/server (e.g. https://psono.test.de/server/info/ shows you some nice json output). 

This is your first OIDC provider that you want to configure (therefore we give it the ID "1").

::: tip
This feature is only available in the Enterprise Edition.
:::

## cidaas

1. Create a new app

    ![cidaas create app](/images/admin/configuration/oidc_cidaas1.png)

2. Configure app

    Configure the scopes and redirect urls as shown below.

    ![cidaas configure app](/images/admin/configuration/oidc_cidaas2.png)

## Server (settings.yaml)

After setting up the IDP for the OIDC-Authentication it is time to configure your running Psono server to act as the SP.
It is required that Psono can reach cidaas and vise versa.


1.  Change or add OIDC configuration in to settings.yaml

   ```yml
   OIDC_CONFIGURATIONS:
       1:
           OIDC_RP_SIGN_ALGO: 'RS256'
           OIDC_RP_CLIENT_ID: 'XXXX'
           OIDC_RP_CLIENT_SECRET: 'XXXX'
           OIDC_OP_JWKS_ENDPOINT: 'https://test.cidaas.de/.well-known/jwks.json'
           OIDC_OP_AUTHORIZATION_ENDPOINT: 'https://test.cidaas.de/authz-srv/authz'
           OIDC_OP_TOKEN_ENDPOINT: 'https://test.cidaas.de/token-srv/token'
           OIDC_OP_USER_ENDPOINT: 'https://test.cidaas.de/users-srv/userinfo'
           OIDC_USERNAME_ATTRIBUTE: 'email'
           OIDC_GROUPS_ATTRIBUTE_DICT_GROUP_ID: 'groupId'
           AUTOPROVISION_PSONO_GROUP: False
           FORCE_MEMBERSHIP_OF_AUTOPROVISIONED_GROUPS: False
           AUTOPROVISION_PSONO_FOLDER: False
   ```

   - The cidaas endpoints can be found on the ".well-known"-page (e.g. https://cidaas.test.de/.well-known/openid-configuration) of your installation
   - Change `AUTOPROVISION_PSONO_GROUP` parameter and set it to true, if you want that Psono automatically creates a corresponding Psono group for any new OIDC group and an appropriate group mapping.
   - Change `AUTOPROVISION_PSONO_FOLDER` parameter and set it to true, if you want that Psono automatically creates a folder with the same name as the group and shares it with the automatically created Psono Group. (Requires `AUTOPROVISION_PSONO_GROUP` to be set to true to have any effect.)
   - Change `FORCE_MEMBERSHIP_OF_AUTOPROVISIONED_GROUPS` parameter and set it to true, if you want that Psono automatically checks "forced membership" for every automatically created Psono Group. (Requires `AUTOPROVISION_PSONO_GROUP` to be set to true to have any effect.)

   ::: tip
   Always restart the server after making changes in the `setting.yml`-file.
   :::


2.  Adjust authentication methods

    Make sure that `OIDC` is part of the `AUTHENTICATION_METHODS` parameter in your settings.yaml e.g.

    ```yaml
    AUTHENTICATION_METHODS: ['OIDC']
    ```
    Restart the server afterward

3.  (optional) Server Secrets

    By default the server will keep a copy of the user's secret keys to allow people to login without a password.
    If you want true client side encryption and as such force users to enter separate password for the encryption you specify
    the following in your settings.yaml. You can also decide later and change that and migrate users during the login
    or apply this setting only to particular users or groups with policies in the Admin Portal.

    ```yaml
    COMPLIANCE_SERVER_SECRETS: 'noone'
    ```

    ::: warning
    If a user loses his password he will lose all his data.
    :::

## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.


1. Basic
   
   Update your config.json similar to the one shown below.
   
   ```json
   {
     ...
       "authentication_methods": ["OIDC"],
       "oidc_provider": [{
         "title": "OIDC Login",
         "provider_id": 1,
         "button_name": "Login "
       }]
     ...
   }
   ```
   
   The variable authentication_methods restricts the allowed login methods. In the example above only OIDC will be allowed
   and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
   to match the one that you used on your server.

2. (optional) Automatic login
   
   You may want to "automatically" click on the login button to initiate the login flow. You can accomplish this by modifying the config.json as shown below:
   
   ```json
   {
     ...
       "authentication_methods": ["OIDC"],
       "auto_login": true,
     ...
   }
   ```
   
   ::: warning
   This will only work if you have just one provider configured with only one authentication method. Users won't be able to modify
   the server url nor choose to register or interact with the login form in any other way.
   :::
