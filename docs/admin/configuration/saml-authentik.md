---
title: SAML - Authentik
metaTitle: Authentik as SAML IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of Authentik as SAML IDP
---

# Authentik as SAML IDP for SSO

## Preamble

The EE server and client support the SAML protocol that allows you to configure an external service as IDP (identity provider)
for SSO (single sign on).
This guide here will explain how to configure Authentik as SAML IDP for SSO. We assume that your server can firewall / network wise access Authentik .
In addition we assume that your webclient is running on https://sub.domain.tld, the server is reachable with
`https://sub.domain.tld/server` (e.g. `https://sub.domain.tld/server/info/` shows you some nice json output). This is your first
SAML provider that you want to configure (therefore we give him the ID "1").

::: tip
This feature is only available in the Enterprise Edition.
:::

## Create provider

As a first step we have to create a provider.

1. Go to Applications -> Providers

    Once there click on the `Create` button

    ![Applications -> Providers](/images/admin/configuration/saml_authentic_1.png)

2. Select type

    Click on `SAML Provider` (not SAML Provider from Metadata)

    ![Select Type](/images/admin/configuration/saml_authentic_1.png)

3. Create SAML Provider

    Fill in the form as shown below. Don't forget to change the domain in the ACS URL accordingly. 

    ![Configure SAML Provider](/images/admin/configuration/saml_authentic_3.png)

    Scroll down, and  under "Advanced protocol settings" select the following.

    ![Configure SAML Provider](/images/admin/configuration/saml_authentic_4.png)

    Afterwards hit `Finish`

## Create Application

Now we have to create an application and connect it to the provider.

1. Go to Applications -> Applications

   Once there click on the `Create` button

   ![Applications -> Applications](/images/admin/configuration/saml_authentic_5.png)

2. Define application details

   Fill out the form as shown below

   ![Applications -> Applications](/images/admin/configuration/saml_authentic_6.png)

   Scroll down and under UI settings enter the correct Launch URL. Adjust the domain accordingly. Afterwards click `Create`

   ![Applications -> Applications](/images/admin/configuration/saml_authentic_7.png)

## Retrieve connection Details

Now that we have a provider and an application, we can get the connection details.

1. Go to Applications -> Providers

   Once there click on the provider

   ![Applications -> Providers](/images/admin/configuration/saml_authentic_8.png)

2. Retrieve connection details

   Download the signing certificate and take note of the `EntityId/Issuer`, `SSO URL (Post)` and `SLO URL (Redirect)` as we will need these details in the next step.

   ![Retrieve connection details](/images/admin/configuration/saml_authentic_9.png)


## Server (settings.yaml)

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Generate SP certificate

    You will need a certificate for your service provider (SP) later. You can generate one easily with:

    ```bash
    openssl req -new -newkey rsa:2048 -x509 -days 3650 -nodes -sha256 -out sp_x509cert.crt -keyout sp_private_key.key
    ```

    This will generate a private key (sp_private_key.key) and the public certificate (sp_x509cert.crt).

2.  Change or add SAML configuration in to settings.yaml.

	```yaml
	SAML_CONFIGURATIONS:
	    1:
	        idp:
	            entityId: "REPLACE_WITH_ENTITY_ID_IDENTIFIER"
	            singleLogoutService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	                url: "REPLACE_WITH_SLO_URL"
	            singleSignOnService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	                url: "REPLACE_WITH_SSO_URL"
	            x509cert: "REPLACE_WITH_SIGNING_CERT"
	            groups_attribute: "http://schemas.xmlsoap.org/claims/Group"
	            username_attribute: "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"
	            email_attribute: "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress"
	            username_domain: "USER_DOMAIN"
	            required_group: []
	            is_adfs: true
	            honor_multifactors: true
	            max_session_lifetime: 43200
	        sp:
	            NameIDFormat: "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"
	            assertionConsumerService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"
	            attributeConsumingService:
	                serviceName: "Psono"
	                serviceDescription: "Psono password manager"
	                requestedAttributes:
	                    -
	                        attributeValue: []
	                        friendlyName: ""
	                        isRequired: false
	                        name: "username"
	                        nameFormat: ""
	            privateKey: "SP_PRIVATE_CERTIFICATE"
	            singleLogoutService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	            x509cert: "SP_X509CERT"
	            autoprovision_psono_folder: false
	            autoprovision_psono_group: false
	        strict: true
	        security:
	            requestedAuthnContext: false
	```

	- Change `USER_DOMAIN` to match your domain.tld
	- Replace `REPLACE_WITH_SSO_URL` SSO URL (Post) from the previous step.
	- Replace `REPLACE_WITH_SLO_URL` SLO URL (Redirect) from the previous step.
	- Replace `REPLACE_WITH_ENTITY_ID_IDENTIFIER` with the Authentik EntitiyId/Issuer from the previous step.
	- Replace `REPLACE_WITH_SIGNING_CERT` with the "Signing certificate" from the previous step. (remove all line breaks)
	- Replace `SP_PRIVATE_CERTIFICATE` with the content of the previous generated "sp_private_key.key". (remove all line breaks)
	- Replace `SP_X509CERT` with the content of the previous generated "sp_x509cert.crt". (remove all line breaks)

    Restart the server afterward

3.  Adjust authentication methods

    Make sure that `SAML` is part of the `AUTHENTICATION_METHODS` parameter in your settings.yaml e.g.

	```yaml
	AUTHENTICATION_METHODS: ['SAML']
	```
    Restart the server afterward

4.  (optional) Server Secrets

    By default the server will keep a copy of the user's secret keys to allow people to login without a password.
    If you want true client side encryption and as such force users to enter separate password for the encryption you specify
    the following in your settings.yaml. You can also decide later and change that and migrate users during the login
    or apply this setting only to particular users or groups with policies in the Admin Portal.

	```yaml
	COMPLIANCE_SERVER_SECRETS: 'noone'
	```

    ::: warning
    If a user loses his password he will lose all his data.
    :::

5.  (optional) Debug Mode

    It is helpful in the later debugging to enable debug mode.

	```yaml
	DEBUG: True
	```

    ::: warning
    Restart the server afterward and don't forget to remove it before going to production.
    :::


## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.


1. Basic
    
    Update your config.json similar to the one shown below.
    
    ```json
    {
      ...
        "authentication_methods": ["SAML"],
        "saml_provider": [{
          "title": "SAML Login",
          "provider_id": 1,
          "button_name": "Login "
        }]
      ...
    }
    ```
    
    The variable authentication_methods restricts the allowed login methods. In the example above only SAML will be allowed
    and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
    to match the one that you used on your server.

2. (optional) Automatic login
    
    You may want to "automatically" click on the login button to initiate the login flow. You can accomplish this by modifying the config.json as shown below:
    
    ```json
    {
      ...
        "authentication_methods": ["SAML"],
        "auto_login": true,
      ...
    }
    ```
    
    ::: warning
    This will only work if you have just one provider configured with only one authentication method. Users won't be able to modify
    the server url nor choose to register or interact with the login form in any other way.
    :::


