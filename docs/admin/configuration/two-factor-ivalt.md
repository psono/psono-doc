---
title: Two Factor - iVALT
metaTitle: Base setup of iVALT two factor | Psono Documentation
meta:
  - name: description
    content: Base setup of iVALT two factor
---

# Base setup of iVALT two factor

## Preamble

The server supports multiple second factors. Some of them need to be configured as an administrator. This guide will
explain how to configure the Psono server to allow users to use iVALT.

## Configuration

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Registration

    register for a iVALT Account first on [ivalt.com](https://ivalt.com/).


2.  Obtain IVALT_SECRET_KEY

    Use the following IVALT_SECRET_KEY for free access: `xT4wonlRJn5DCuoai3e6C7OpXOIFhbG85sykvAt9`

3.  Add iVALT API credentials to setting.yml

    Once you receive the IVALT_SECRET_KEY, add it to settings.yaml and include "ivalt" in the ALLOWED_SECOND_FACTORS list.

    ```yaml
    ALLOWED_SECOND_FACTORS: ['yubikey_otp', 'google_authenticator', 'duo', 'webauthn', 'ivalt']
    IVALT_SECRET_KEY: 'xT4wonlRJn5DCuoai3e6C7OpXOIFhbG85sykvAt9'
    ```

    Replace `IVALT_SECRET_KEY` with the API key that was provided to you by iVALT.

    Restart the server afterward
