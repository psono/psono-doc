---
title: OIDC - WSO2 Identity Server
metaTitle: WSO2 Identity Server as OIDC IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of WSO2 Identity Server as OIDC IDP
---

# WSO2 Identity Server as IDP for OIDC-SSO

To set up the IDP you need a running instance of WSO2 Identity Server with a configurable realm.

## Preamble

The Enterprise Edition (EE) server and client support the OIDC protocol that allows you to configure an external service
as IDP (identity provider) for SSO (single sign on). This guide here will explain how to configure WSO2 Identity Server as OIDC-IDP for SSO. We assume that:

* your WSO2 Identity Server instance is running on https://wso2.example.com,
* your webclient can be accessed on https://psono.example.com
* the server is reachable at https://psono.example.com/server (e.g. https://psono.example.com/server/info/ shows you some nice json output). 

This is your first OIDC provider that you want to configure (therefore we give it the ID "1").

::: tip
This feature is only available in the Enterprise Edition.
:::

## WSO2 Identity Server

1. Navigate to Service Providers and click on `Add`.
2. Enter a nice name and description

    ![Name and describe Service provider](/images/admin/configuration/oidc_wso2_screen01.png)

3. Configure claims as shown below

    ![Name and describe Service provider](/images/admin/configuration/oidc_wso2_screen02.png)

4. Configure OpenID Connect

    ![Click on Configure](/images/admin/configuration/oidc_wso2_screen03.png)

4. Adjust config

    Enter `https://psono.example.com/server/oidc/1/callback/` as your callback url. Adjust the domain according to your setup.
    
    ![Click on Configure](/images/admin/configuration/oidc_wso2_screen04.png)

5. Copy `OAuth Client Key` and `OAuth Client Secret`

    We will need these two parameters later.

    ![Copy WSO2 Client key and secret](/images/admin/configuration/oidc_wso2_screen05.png)


## Server (settings.yaml)

After setting up the IDP for the OIDC-Authentication it is time to configure your running Psono server to act as the SP.
It is required that Psono can reach WSO2 Identity Server and vise versa.


1.  Change or add OIDC configuration in to settings.yaml

  ```yml
  OIDC_CONFIGURATIONS:
      1:
          OIDC_RP_SIGN_ALGO: 'RS256'
          OIDC_RP_CLIENT_ID: '9ydFzCGsjR5_fkaWuBij3S8OfWga'
          OIDC_RP_CLIENT_SECRET: 'Z_1hBkB7QCU71XiPhfcI7bzTCksa'
          OIDC_USERNAME_ATTRIBUTE: 'sub'
          OIDC_OP_JWKS_ENDPOINT: 'https://wso2.example.com/oauth2/jwks'
          OIDC_OP_AUTHORIZATION_ENDPOINT: 'https://wso2.example.com/oauth2/authorize'
          OIDC_OP_TOKEN_ENDPOINT: 'https://wso2.example.com/oauth2/token'
          OIDC_OP_USER_ENDPOINT: 'https://wso2.example.com/oauth2/userinfo?schema=openid'
          AUTOPROVISION_PSONO_GROUP: False
          FORCE_MEMBERSHIP_OF_AUTOPROVISIONED_GROUPS: False
          AUTOPROVISION_PSONO_FOLDER: False
  ```
   
  - Replace `OIDC_RP_CLIENT_ID` and `OIDC_RP_CLIENT_SECRET` with the Client key and secret
  - Replace the domain of all `OIDC_OP_...`-parameters to match your domain
  - Change `AUTOPROVISION_PSONO_GROUP` parameter and set it to true, if you want that Psono automatically creates a corresponding Psono group for any new OIDC group and an appropriate group mapping.
  - Change `AUTOPROVISION_PSONO_FOLDER` parameter and set it to true, if you want that Psono automatically creates a folder with the same name as the group and shares it with the automatically created Psono Group. (Requires `AUTOPROVISION_PSONO_GROUP` to be set to true to have any effect.)
  - Change `FORCE_MEMBERSHIP_OF_AUTOPROVISIONED_GROUPS` parameter and set it to true, if you want that Psono automatically checks "forced membership" for every automatically created Psono Group. (Requires `AUTOPROVISION_PSONO_GROUP` to be set to true to have any effect.)


  ::: tip
  Always restart the server after making changes in the `setting.yml`-file.
  :::


2.  Adjust authentication methods

    Make sure that `OIDC` is part of the `AUTHENTICATION_METHODS` parameter in your settings.yaml e.g.

    ```yaml
    AUTHENTICATION_METHODS: ['OIDC']
    ```

    Restart the server afterward

3.  (optional) Server Secrets

    By default the server will keep a copy of the user's secret keys to allow people to login without a password.
    If you want true client side encryption and as such force users to enter separate password for the encryption you specify
    the following in your settings.yaml. You can also decide later and change that and migrate users during the login
    or apply this setting only to particular users or groups with policies in the Admin Portal.

    ```yaml
    COMPLIANCE_SERVER_SECRETS: 'noone'
    ```

    ::: warning
    If a user loses his password he will lose all his data.
    :::

## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.


1. Basic
   
   Update your config.json similar to the one shown below.
   
   ```json
   {
     ...
       "authentication_methods": ["OIDC"],
       "oidc_provider": [{
         "title": "OIDC Login",
         "provider_id": 1,
         "button_name": "Login "
       }]
     ...
   }
   ```
   
   The variable authentication_methods restricts the allowed login methods. In the example above only OIDC will be allowed
   and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
   to match the one that you used on your server.

2. (optional) Automatic login
   
   You may want to "automatically" click on the login button to initiate the login flow. You can accomplish this by modifying the config.json as shown below:
   
   ```json
   {
     ...
       "authentication_methods": ["OIDC"],
       "auto_login": true,
     ...
   }
   ```
   
   ::: warning
   This will only work if you have just one provider configured with only one authentication method. Users won't be able to modify
   the server url nor choose to register or interact with the login form in any other way.
   :::