---
title: SAML - Google Workspace
metaTitle: Google Workspace as SAML IDP for SSO | Psono Documentation
meta:
  - name: description
    content: Configuration of Google Workspace as SAML IDP
---

# Google Workspace as SAML IDP for SSO

## Preamble

The EE server and client support the SAML protocol that allows you to configure an external service as IDP (identity provider)
for SSO (single sign on).
This guide here will explain how to configure Google Workspace as SAML IDP for SSO. We assume that Google Workspace can firewall / network wise access your server.
In addition we assume that your webclient is running on https://example.com, the server is reachable with
`https://example.com/server` (e.g. `https://example.com/server/info/` shows you some nice json output). This is your first
SAML provider that you want to configure (therefore we give him the ID "1").

::: tip
This feature is only available in the Enterprise Edition.
:::

## Google Workspace

As a first step we have to configure Google Workspace.

1.  Sign in to Google Admin console

    Sign in to [Google Admin console](https://admin.google.com) with an account with super administrator privileges.

2. Go to Apps > Web and mobile apps

    Click on "Add App" and choose "Add custom SAML app".
    
    ![Add custom SAML app](/images/admin/configuration/saml_google_workspaces_add_custom_saml_app_step_1.png)
    
3. Configure "Name"

    Enter a suitable name for your application and (optional) add a logo. Afterwards click on "continue"

    ![Configure name and logo](/images/admin/configuration/saml_google_workspaces_enter_name_and_logo_step_2.png)

4. Write down "Identity provider details"

    Write down the following three parameters as we need them in a minute.

    ![Google Workspaces Identity provider details](/images/admin/configuration/saml_google_workspaces_copy_identity_provider_info_step_3.png)

5. Configure "Service provider details"

    Enter the details as shown below. Adjust the domain according to your setup.

    ![Google Workspaces Service provider details](/images/admin/configuration/saml_google_workspaces_enter_service_provider_details_step_4.png)

6. Configure "Attribute mapping"

    Configure both our required attributes as shown below

    ![Google Workspaces Attribute Mapping](/images/admin/configuration/saml_google_workspaces_configure_attribute_mapping_step_5.png)

7. Configure "User Access"

    Now we have to select the users (or groups) who will be allowed to login to Psono. Click on this "Arrow down" icon next to "User acces"

    ![Configure user access](/images/admin/configuration/saml_google_workspaces_user_access_step_6.png)
    
    For our demo we select "Everyone" yet you can restrict access to certain groups if you want.a
    
    ![Select everyone](/images/admin/configuration/saml_google_workspaces_user_access_allow_everyone_step_7.png)

## Server (settings.yaml)

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Generate SP certificate

    You will need a certificate for your service provider (SP) later. You can generate one easily with:

    ```bash
    openssl req -new -newkey rsa:2048 -x509 -days 3650 -nodes -sha256 -out sp_x509cert.crt -keyout sp_private_key.key
    ```

    This will generate a private key (sp_private_key.key) and the public certificate (sp_x509cert.crt).

2.  Change or add SAML configuration in to settings.yaml.
    Comments inside the config:

	```yaml
	SAML_CONFIGURATIONS:
	    1:
	        idp:
	            entityId: "REPLACE_WITH_ENTITY_ID"
	            singleLogoutService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	                url: "https://login.microsoftonline.com/common/wsfederation?wa=wsignout1.0"
	            singleSignOnService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	                url: "REPLACE_WITH_SSO_URL"
	            x509cert: "CERT_FROM_IDENTITY_PROVIDER"
	            groups_attribute: "groups"
	            username_attribute: "username"
	            email_attribute: "email"
	            username_domain: "USER_DOMAIN"
	            required_group: []
	            is_adfs: true
	            honor_multifactors: true
	            max_session_lifetime: 43200
	        sp:
	            NameIDFormat: "urn:oasis:names:tc:SAML:2.0:nameid-format:persistent"
	            assertionConsumerService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"
	            attributeConsumingService:
	                serviceName: "Psono"
	                serviceDescription: "Psono password manager"
	                requestedAttributes:
	                    -
	                        attributeValue: []
	                        friendlyName: ""
	                        isRequired: false
	                        name: "username"
	                        nameFormat: ""
	            privateKey: "SP_PRIVATE_CERTIFICATE"
	            singleLogoutService:
	                binding: "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"
	            x509cert: "SP_X509CERT"
	            autoprovision_psono_folder: false
	            autoprovision_psono_group: false
	        strict: true
	        security:
	            requestedAuthnContext: false
	```

	- Change `serviceName` to match the name of your Google Workspaces
	- Change `USER_DOMAIN` to match your domain.tld
	- Replace `REPLACE_WITH_SSO_URL` with the "SSO Url" from the Identity Provider details usually something like https://accounts.google.com/...
	- Replace `REPLACE_WITH_ENTITY_ID` with the "Entity ID" from the Identity Provider details usually something like https://accounts.google.com/...
	- Replace `CERT_FROM_IDENTITY_PROVIDER` with the "Certificate" from previous Identity Provider details configuration. (remove all line breaks)
	- Replace `SP_PRIVATE_CERTIFICATE` with the content of the previous generated "sp_private_key.key". (remove all line breaks)
	- Replace `SP_X509CERT` with the content of the previous generated "sp_x509cert.crt". (remove all line breaks)

    Restart the server afterward

3.  Adjust authentication methods

    Make sure that `SAML` is part of the `AUTHENTICATION_METHODS` parameter in your settings.yaml e.g.

	```yaml
	AUTHENTICATION_METHODS: ['SAML']
	```
    Restart the server afterward

4.  (optional) Server Secrets

    By default the server will keep a copy of the user's secret keys to allow people to login without a password.
    If you want true client side encryption and as such force users to enter separate password for the encryption you specify
    the following in your settings.yaml. You can also decide later and change that and migrate users during the login
    or apply this setting only to particular users or groups with policies in the Admin Portal.

    ```yaml
    COMPLIANCE_SERVER_SECRETS: 'noone'
    ```

    ::: warning
    If a user loses his password he will lose all his data.
    :::

5.  (optional) Debug Mode

    It is helpful in the later debugging to enable debug mode.

	```yaml
	DEBUG: True
	```

    ::: warning
    Restart the server afterward and don't forget to remove it before going to production.
    :::

## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.


1. Basic
    
    Update your config.json similar to the one shown below.
    
    ```json
    {
      ...
        "authentication_methods": ["SAML"],
        "saml_provider": [{
          "title": "SAML Login",
          "provider_id": 1,
          "button_name": "Login "
        }]
      ...
    }
    ```
    
    The variable authentication_methods restricts the allowed login methods. In the example above only SAML will be allowed
    and the normal login "hidden". The title and button_name can be adjusted however you like. The `provider_id` needs
    to match the one that you used on your server.

2. (optional) Automatic login
    
    You may want to "automatically" click on the login button to initiate the login flow. You can accomplish this by modifying the config.json as shown below:
    
    ```json
    {
      ...
        "authentication_methods": ["SAML"],
        "auto_login": true,
      ...
    }
    ```
    
    ::: warning
    This will only work if you have just one provider configured with only one authentication method. Users won't be able to modify
    the server url nor choose to register or interact with the login form in any other way.
    :::

