---
title: Email - Amazon SES
metaTitle: Email configuration with Amazon SES | Psono Documentation
meta:
  - name: description
    content: Configuration of email delivery with Amazon SES
---

# Email configuration with Amazon SES

## Preamble

The server supports multiple email providers. This guide will explain how to configure the Psono server to use Amazon SES for
email delivery.

## Configuration

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Configure email address

	```yaml
	EMAIL_FROM: 'something@example.com'
	```

	All emails that are sent by the server will come from this email address.

    Restart the server afterward.

2.  Add Amazon SES API credentials to setting.yml

	```yaml
	EMAIL_BACKEND: 'anymail.backends.amazon_ses.EmailBackend'
	AMAZON_SES_CLIENT_PARAMS_ACCESS_KEY_ID: 'YOUR_AMAZON_SES_CLIENT_PARAMS_ACCESS_KEY_ID'
	AMAZON_SES_CLIENT_PARAMS_SECRET_ACCESS_KEY: 'YOUR_AMAZON_SES_CLIENT_PARAMS_SECRET_ACCESS_KEY'
	AMAZON_SES_CLIENT_PARAMS_REGION_NAME: 'YOUR_AMAZON_SES_CLIENT_PARAMS_REGION_NAME'
	```

	- Replace `YOUR_AMAZON_SES_CLIENT_PARAMS_ACCESS_KEY_ID` with the access key id that was provided to you by Amazon.
	- Replace `YOUR_AMAZON_SES_CLIENT_PARAMS_SECRET_ACCESS_KEY` with the secret access key that was provided to you by Amazon.
	- Replace `YOUR_AMAZON_SES_CLIENT_PARAMS_REGION_NAME` with the region name that was provided to you by Amazon, e.g. us-west-2

    Restart the server afterward.


## Testing

To send a test email to `something@something.com` execute:

```bash
docker run --rm \
  -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
  -ti psono/psono-combo:latest python3 ./psono/manage.py sendtestmail something@something.com
```

If you receive this test email, then email should be configured proper.


## More Information

Psono is using Anymail under the hood. You can check out the official documentation here:

[anymail.readthedocs.io/en/stable/esps/amazon_ses/](https://anymail.readthedocs.io/en/stable/esps/amazon_ses/)
