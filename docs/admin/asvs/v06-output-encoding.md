---
title: V6 Output Encoding
metaTitle: V6 Output Encoding | Psono Documentation
meta:
  - name: description
    content: Output encoding / escaping
---

# V6 Output Encoding

Output encoding / escaping

[[toc]]

This section was incorporated into V5 in Application Security Verification Standard 2.0.
ASVS requirement 5.16 addresses contextual output encoding to help prevent Cross Site
Scripting.
