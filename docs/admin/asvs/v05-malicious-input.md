---
title: V5 Malicious Input
metaTitle: V5 Malicious Input | Psono Documentation
meta:
  - name: description
    content: Malicious input handling verification requirements
---

# V5 Malicious Input

Malicious input handling verification requirements

[[toc]]

## ASVS Verification Requirement

| ID                          | Detailed Verification Requirement                                                                                                                                                                                                                                                                                                                              | Level 1                 | Level 2    | Level 3     | Since     |
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------|
| 5.1                         | Verify that the runtime environment is not susceptible to buffer overflows, or that security controls prevent buffer overflows.                                                                                                                                                                                                                                | x                       | x          | x           | 1.0       |
| 5.3                         | Verify that server side input validation failures result in request rejection and are logged.                                                                                                                                                                                                                                                                  | x                       | x          | x           | 1.0       |
| 5.5                         | Verify that input validation routines are enforced on the server side.                                                                                                                                                                                                                                                                                         | x                       | x          | x           | 1.0       |
| 5.6                         | Verify that a single input validation control is used by the application for each type of data that is accepted.                                                                                                                                                                                                                                               |                         |            | x           | 1.0       |
| 5.10                        | Verify that all SQL queries, HQL, OSQL, NOSQL and stored procedures, calling of stored procedures are protected by the use of prepared statements or query parameterization, and thus not susceptible to SQL injection                                                                                                                                         | x                       | x          | x           | 2.0       |
| 5.11                        | Verify that the application is not susceptible to LDAP Injection, or that security controls prevent LDAP Injection.                                                                                                                                                                                                                                            | x                       | x          | x           | 2.0       |
| 5.12                        | Verify that the application is not susceptible to OS Command Injection, or that security controls prevent OS Command Injection.                                                                                                                                                                                                                                | x                       | x          | x           | 2.0       |
| 5.13                        | Verify that the application is not susceptible to Remote File Inclusion (RFI) or Local File Inclusion (LFI) when content is used that is a path to a file.                                                                                                                                                                                                     | x                       | x          | x           | 3.0       |
| 5.14                        | Verify that the application is not susceptible to common XML attacks, such as XPath query tampering, XML External Entity attacks, and XML injection attacks.                                                                                                                                                                                                   | x                       | x          | x           | 2.0       |
| 5.15                        | Ensure that all string variables placed into HTML or other web client code is either properly contextually encoded manually, or utilize templates that automatically encode contextually to ensure the application is not susceptible to reflected, stored and DOM Cross-Site Scripting (XSS) attacks.                                                         | x                       | x          | x           | 2.0       |
| 5.16                        | If the application framework allows automatic mass parameter assignment (also called automatic variable binding) from the inbound request to a model, verify that security sensitive fields such as “accountBalance”, “role” or “password” are protected from malicious automatic binding.                                                                     |                         | x          | x           | 2.0       |
| 5.17                        | Verify that the application has defenses against HTTP parameter pollution attacks, particularly if the application framework makes no distinction about the source of request parameters (GET, POST, cookies, headers, environment, etc.)                                                                                                                      |                         | x          | x           | 2.0       |
| 5.18                        | Verify that client side validation is used as a second line of defense, in addition to server side validation.                                                                                                                                                                                                                                                 |                         | x          | x           | 3.0       |
| 5.19                        | Verify that all input data is validated, not only HTML form fields but all sources of input such as REST calls, query parameters, HTTP headers, cookies, batch files, RSS feeds, etc; using positive validation (whitelisting), then lesser forms of validation such as greylisting (eliminating known bad strings), or rejecting bad inputs (blacklisting).   |                         | x          | x           | 3.0       |
| 5.20                        | Verify that structured data is strongly typed and validated against a defined schema including allowed characters, length and pattern (e.g. credit card numbers or telephone, or validating that two related fields are reasonable, such as validating suburbs and zip or post codes match).                                                                   |                         | x          | x           | 3.0       |
| 5.21                        | Verify that unstructured data is sanitized to enforce generic safety measures such as allowed characters and length, and characters potentially harmful in given context should be escaped (e.g. natural names with Unicode or apostrophes, such as ?? or O'Hara)                                                                                              |                         | x          | x           | 3.0       |
| 5.22                        | Make sure untrusted HTML from WYSIWYG editors or similar are properly sanitized with an HTML sanitizer and handle it appropriately according to the input validation task and encoding task.                                                                                                                                                                   | x                       | x          | x           | 3.0       |
| 5.23                        | For auto-escaping template technology, if UI escaping is disabled, ensure that HTML sanitization is enabled instead.                                                                                                                                                                                                                                           |                         | x          | x           | 3.0       |
| 5.24                        | Verify that data transferred from one DOM context to another, uses safe JavaScript methods, such as using .innerText and .val.                                                                                                                                                                                                                                 |                         | x          | x           | 3.0       |
| 5.25                        | Verify when parsing JSON in browsers, that JSON.parse is used to parse JSON on the client. Do not use eval() to parse JSON on the client.                                                                                                                                                                                                                      |                         | x          | x           | 3.0       |
| 5.26                        | Verify that authenticated data is cleared from client storage, such as the browser DOM, after the session is terminated.                                                                                                                                                                                                                                       |                         | x          | x           | 3.0       |


### 5.1

Buffer overflows are highly unlikely in Python and JavaScript. Should a buffer overflow in one of the relying C libraries
underneath of the python code happen, then the Python code will securely fail and not expose any sensitive data.

### 5.3

All input parameters are checked, validated and sanitized in so called "serializers" and will result in request rejections.
The EE server will log those events.

### 5.5

Serializers are enforced on the server

### 5.6

The definitions of fields are centrally implemented and parameterized

### 5.10

Psono (wherever possible) uses Django's ORM to query the database. Wherever that was not possible, it has been verified
that the SQL queries have been proper parameterized.

### 5.11

LDAP is only part of the EE. All input parameters, provided by the user (e.g. username or password) are sanitized and
(wherever possible) only used in parameterized queries.

### 5.12

No OS command (e.g. shell, subproces, ...) is used.

### 5.13

Psono does not interpret any files under control of an attacker nor use any risky operations (e.g. xml validation).

### 5.14

Psono does not use XML

### 5.15

Multiple measures are in place to prevent this. The webservers XSS and CSP protection is enabled.
The angularjs framework used by Psonos webclient has has proper escaping of all user content.
The server does not return any input value provided to him and minimizes the risks of reflected XSS in error responses

### 5.16

Psono does not user implicit mass assignments and instead uses explicit assignments to the underlying fields of the model.
Appropriate validations are in place to protect data that should not be altered.

### 5.17

This possible issue is very hard to find / anticipate and prevent in the end, but Psono does all it can.
All parameters are protected by the transport encryption that prevents men-in-the-middle to temper with the data.
All parameters behind the transport encryption layer are presented as a JSON object. The JSON deserialization will securely
fail for all malformed data inputs. The Django Restframework that handles the request afterward and the "serializer"
layer coming afterward are to my knowledge not exploitable by HTTP parameter pollution.

### 5.18 (violation)

Not implemented. The client does not validate all data that it sends to the server.

### 5.19

All input data is validated against a fix set of data types first. Afterwards the serialzer layer will take care that no
mal formed input is passed on.

### 5.20 (violation)

All REST input data is strongly typed, yet there is room for improvement.

### 5.21 (violation)

All REST input data is strongly typed, yet there is room for improvement.

### 5.22

Angularjs takes care of all sanitizing. The server, as it never gets the clear text data, has no chance to implement
any restrictions / rules here.

### 5.23

Verified. Angularjs sanitizer escapes proper as far as one could test.

### 5.24

Unsafe functions have been avoided. Automatic checks are in place to report usage and fail the build if used.

### 5.25

JSON.parse is used, not eval.

### 5.26

Once the session is terminated the client, in its logout procedure, deletes all local data (besides a persistant datastore
that is kept when the user clicked "remember me")




