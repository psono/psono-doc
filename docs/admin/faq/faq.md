---
title: FAQ
metaTitle: FAQ | Psono Documentation
meta:
  - name: description
    content: FAQs concerning the installation and configuration
---

# FAQ

FAQs concerning the installation and configuration

[[toc]]


### Do you have an easier way to install Psono and try it out locally?

We offer our [quickstart](https://gitlab.com/psono/psono-quickstart) script that will setup a local demo environment in a couple of minutes.

::: warning
This script is only meant for demo purposes and not for a production grade system.
:::

### How to install the enterprise server without docker?

We currently do not provide the possibility to install the enterprise server without docker (baremetal). The baremetal installation
is meant for developers who can deal with python dependency issues and so on. So this has mainly two reasons:

1) The pain to update Psono regulary would be too high for administrators

2) The pain for us to deal with incorrect version libraries or system dependencies is too high for us to support.

### How to resolve "License not provided, and license servers unreachable"?

The Psono Enterprise Edition server tries to connect to https://license01.psono.com on startup. It will send the PUBLIC_KEY to the server
and the license server responds with a license for 10 users. The error usually means that the server was unable to reach the license server.

You can check that the license server is available by opening this URL [https://license01.psono.com/info/](https://license01.psono.com/info/) in your browser.

This will most likely work indicating some kind of network (DNS, routing, ...) or firewall issue on your server.

You can check if the server in general can reach the license server with this command:

```bash
curl https://license01.psono.com/info/
```

::: tip
If this command fails, check with your network administrator. Be aware that license01.psono.com sits behind cloudflare so IP whitelisting is impossible.
:::

and this one to test from within your docker container:

```bash
docker run psono/psono-combo-enterprise:latest curl https://license01.psono.com/info/
```

::: tip
If the first curl command worked and this one failed, then this is most likely a local missconfiguration. e.g. local firewall or iptables or docker networking issue.
:::

If both commands above work, then try with your actual settings.yml

```bash
docker run -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml psono/psono-combo-enterprise:latest curl https://license01.psono.com/info/
```

::: tip
If you receive an error here, then this usually means that you have some kind of problem with your settings.yml. A potential cause could be cryptographic parameters (SECRET, PRIVATE or PUBLIC keys) not being generated with
the `python3 ./psono/manage.py generateserverkeys` command.
:::

### Can I run Psono server enterprise offline without internet connection?

Yes, yet you have to do some additional configuration.

- Connection to license server: You can ask us to provide you an offline license code. Please send us an email to support@psono.com with your PUBLIC_KEY (it is part of your settings.yml) and we generate you an offline license code.

- Connection to a timeserver: Psono requires a NTP time server to be available. By default it is using time.google.com but it can be adjusted with the TIME_SERVER variable in your settings.yml

- Connection to Yubico Server: If you want to use YubiKey second factor you will have to make sure that your server can reach api*.yubico.com or configure an own YubiKey server with the YUBICO_API_URLS variable

- Connection to Duo Server: If you want to use Duo second factor you will have to make sure that your server can reach api*.duosecurity.com.

### How do I fix "OCI runtime create failed"-Error?

This error usually has two potential reasons:

1) Incorrect path: Make sure that all paths you have specified in the docker command are correct.

2) Incorrect permissions: Make sure that no file permissions or SELINUX is blocking access to the files you mentioned in your command.

### Psono's admin portal still reports the old version of the client, How can I force the Portal to show the new version?

Psono's portal is asking the server for the `WEBCLIENT_URL` parameter and will use that one to fetch the so called `VERSION.txt`
from the webclient. That `VERSION.txt` is usually cached in your browser together with the rest of the client by a so
called "service worker", which powers Psono's offline mode. The service worker will pickup the new version of the client with
the next restart of the browser, yet you can also force the service worker to refresh it's cache.
Check out our guide [How do I force the service worker to refresh it's cache?](#how-do-i-force-the-service-worker-to-refresh-it-s-cache)

### Psono's Webclient is not reflecting the changes done to the config.json. How can I force the Webclient to show the new version?

The `config.json`, together with the rest of the webclient is usually cached in your browser by a so called "service worker",
which powers Psono's offline mode. Whenever you deploy a new version, the "service worker" will notice that a new version
exists and reloads its cache. This sadly doesn't work for the config.json, which can be in particular troubling during
the initial installation, where your changes to the `config.json` are not reflected in the browser.
You can also force the service worker to refresh it's cache.
Check out our guide [How do I force the service worker to refresh it's cache?](#how-do-i-force-the-service-worker-to-refresh-it-s-cache)

### How do I force the service worker to refresh it's cache?

There might be a time where you need to manually refresh the cache of the service worker. e.g. you adjust your config.json
or want to force the webclient to reload the client from the server. To refresh the service worker's cache open the webclient
in your Chrome and open your Chrome's developer tools (Windows shortcut `F12`). Go to `Application` > `Service Workers`
and then click on `Unregister`.

![How to force the service worker to refresh it's cache](/images/admin/faq/how_to_force_service_worker_cache_reload.jpg)

Afterwards reload the page (Windows shortcut `F5`, MacOS shurtcut `CMD + R`). Now you should see
an up to date version of your webclient and it's configuration.


### How do I fix "non_field_errors"-Error when using SAML?

This error is extremely broad. The first thing that you should do is to enable DEBUG mode on your server with the settings.yml

Afterwards you should see a more detailed error.

### How do I fix "Invalid issuer in Assertion/Response ..."-Error when using SAML?

You have to adjust the entityId in your settings.yml to match the one from the error message. Afterwards don't forget to restart the server.

### How do I fix "Signature validation failed. SAML Response rejected"-Error when using SAML?

This error indicates that the IDP certificate that you configured in your settings.yml is incorrect.
Make sure that it has the right encoding (BASE64). After adjusting the settings.yml don't forget to restart the server.

### How do I fix "SAML_EMAIL_ATTRIBUTE_NOT_IN_ATTRIBUTES"-Error when using SAML?

You have to check the claims / attributes returned by the server to see if the name of the email attribute matches the one you configured in your SAML configuration in your settings.yml.

You can debug the response from the server by using one of many SAML debug extensions, e.g.:
[https://chrome.google.com/webstore/detail/saml-tracer/mpdajninpobndbfcldcmbpnnbhibjmch](https://chrome.google.com/webstore/detail/saml-tracer/mpdajninpobndbfcldcmbpnnbhibjmch)

Check that your "email_attribute" in your settings.yml matches the one that you see in the debug extension and don't forget to restart the server after adjusting the settings.yml

### How do I fix "SAML_USERNAME_ATTRIBUTE_NOT_IN_ATTRIBUTES"-Error when using SAML?

You have to check the claims / attributes returned by the server to see if the name of the username attribute matches the one you configured in your SAML configuration in your settings.yml.

You can debug the response from the server by using one of many SAML debug extensions, e.g.:
[https://chrome.google.com/webstore/detail/saml-tracer/mpdajninpobndbfcldcmbpnnbhibjmch](https://chrome.google.com/webstore/detail/saml-tracer/mpdajninpobndbfcldcmbpnnbhibjmch)

Check that your "username_attribute" in your settings.yml matches the one that you see in the debug extension and don't forget to restart the server after adjusting the settings.yml

### Do you have a docker-compose file?

Yes of course, yet please be aware that this file is highly opinionated and may not suit your need.

This docker-compose assumes the `config.json` and `settings.yaml` in `./config/`. The Postgres data will be stored in `./data/postgres/`.
The docker-compose will expose Psono on `TCP 10200`.

```yaml
version: '3.8'
services:
  psono-database:
    container_name: psono-database
    image: postgres:13-alpine
    labels:
      - "com.centurylinklabs.watchtower.enable=true"
    environment:
      POSTGRES_USER: psono
      POSTGRES_PASSWORD: REPLACE_ME_WITH_SOMETHING_SECURE
    volumes:
      - type: bind
        source: ./data/postgres
        target: /var/lib/postgresql/data
    restart: always
   
  psono-combo:
    container_name: psono-combo
    image: psono/psono-combo:latest
    ports:
      - "10200:80"
    labels:
      - "com.centurylinklabs.watchtower.enable=true"
    depends_on:
      - psono-database
    links:
      - postgres:psono-database
    command: sh -c "sleep 10 && /bin/sh /root/configs/docker/cmd.sh"
    volumes:
      - type: bind
        source: ./config/server-settings.yaml
        target: /root/.psono_server/settings.yaml
      - type: bind
        source: ./config/config.json
        target: /usr/share/nginx/html/config.json
      - type: bind
        source: ./config/config.json
        target: /usr/share/nginx/html/portal/config.json
    sysctls:
      - net.core.somaxconn=65535
    restart: always
  

# Comment in the following lines if you want to have automatic updates
#  psono-watchtower:
#    container_name: psono-watchtower
#    restart: always
#    image: containrrr/watchtower
#    command: --label-enable --cleanup --interval 3600
#    labels:
#      - "com.centurylinklabs.watchtower.enable=true"
#    volumes:
#      - /var/run/docker.sock:/var/run/docker.sock

```

### Do you have a helm chart?

We have one that you can use as inspiration: [gitlab.com/psono/psono-helm-chart](https://gitlab.com/psono/psono-helm-chart)

### Is Psono HIPAA compliant?

The Health Insurance Portability and Accountability Act (HIPAA) demands certain security and privacy standards to protect patient health information.
Psono is no so called HIPAA "Covered Entity" as we are no healthcare provider, clearinghouse, ... and do not store
any "Protected Health Information" (PHI). We gathered some more data about [HIPAA compliant password managers](https://psono.com/blog/hipaa-compliant-password-manager)
and how Psono can help to achieve HIPAA compliance.

### Is Psono password manager GDPR compliant?

Psono is fully GDPR compliant. Details about Psono's privacy approach can be found in [Psono's privacy policy](https://www.psono.pw/privacy-policy.html).


### Is Psono password manager CCPA compliant?

Psono is fully CCPA compliant. Details about Psono's privacy approach can be found in [Psono's privacy policy](https://www.psono.pw/privacy-policy.html).

### Is Psono password manager SOC 2 / SOC 3 compliant?

Psono has not yet been audited under SOC 2 / SOC 3, yet Psono is only using SOC 2 / SOC 3 compliant service providers (e.g. Google Cloud Platform) for it's infrastructure like network, servers, loadbalancing and databasaes.
Some more details about Psono's security approach can be found here in [Psono's security guide](https://psono.com/security) or [Psono's ASVS audit](/admin/asvs/overview.html)

