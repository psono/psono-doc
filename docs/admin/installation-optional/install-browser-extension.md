---
title: Install Browser Extension (optional)
metaTitle: Install Psono Browser Extension | Psono Documentation
meta:
  - name: description
    content: Installation guide for the Psono browser extensions for Chrome, Edge or Firefox.
---

# Install Browser Extension

Installation guide for the Psono browser extensions for Chrome or Firefox.

To install one of our browser extensions, follow the appropriate guide below.

## Install Chrome extension

Open your Chrome browser and visit the following url:

[chrome.google.com/webstore/detail/psonopw/eljmjmgjkbmpmfljlmklcfineebidmlo](https://chrome.google.com/webstore/detail/psonopw/eljmjmgjkbmpmfljlmklcfineebidmlo)

## Install Edge extension

Open your Edge browser and visit the following url:

[microsoftedge.microsoft.com/addons/detail/psono-free-password-man/abobmepfpbkapdlmfhnnkebcnhgeccbm](https://microsoftedge.microsoft.com/addons/detail/psono-free-password-man/abobmepfpbkapdlmfhnnkebcnhgeccbm)


## Install Firefox extension

Open your Firefox browser and visit the following url:

[addons.mozilla.org/firefox/addon/psono-pw-password-manager/](https://addons.mozilla.org/firefox/addon/psono-pw-password-manager/)


## Install company wide

You may want to install the browser extension for your users centrally. e.g. with Intune. You can find a guide here:

[doc.psono.com/admin/configuration/intune.html](/admin/configuration/intune.html)


## Configuration

If you are using SAML or OIDC you may need to configure your browser extension. A Guide how to configure browser extensions
can be found here:

[doc.psono.com/admin/configuration/browser-extension.html](/admin/configuration/browser-extension.html)

