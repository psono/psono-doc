---
title: 3. Install Webclient (optional)
metaTitle: Install Psono Webclient | Psono Documentation
meta:
  - name: description
    content: Installation guide of the Psono Webclient
  - name: robots
    content: noindex, nofollow
---

# Install Webclient

Installation guide of the Psono Webclient

## Preamble

The webclient is a stateless website, allowing users to access their passwords without the need to install anything
(besides a browser). It can be served by any web server, we prefer Nginx, but any other webserver (Apache, IIS) is fine too.
The web client is optional. As an alternative users can use the official Psono browser extensions.

## Installation with Docker

The latest build of our Web Client as a docker image can be found here: [hub.docker.com/r/psono/psono-client/](https://hub.docker.com/r/psono/psono-client/)
Follow belows instructions to bring it online.

1. Create a `config.json` with the following content in e.g. `/opt/docker/psono-client/`:

    ```json
    {
      "backend_servers": [{
        "title": "Psono.pw",
        "url": "https://psono.example.com/server"
      }],
      "base_url": "https://psono.example.com/",
      "allow_custom_server": true,
      "allow_registration": true,
      "allow_lost_password": true,
      "disable_download_bar": false,
      "remember_me_default": false,
      "trust_device_default": false,
      "authentication_methods": ["AUTHKEY", "LDAP"],
      "saml_provider": []
    }
    ```

    Adjust the title and URLs according to your setup. The backend_server url should be the url where you see the line below when you append /info/ to it:

    {"info":"{\"version\": \"....}

2. (optional) Change domain for login

    You don't see it yet, but later in the login form your username will be a composition of something that the user
    defines, and a domain, forming the full username that looks similar to an email address (e.g. something@example.com).

    The client will parse the `base_url` parameter from the `config.json`. Sometimes you would want to specify a
    different domain. You can do that by adjusting the `config.json` as shown below:

    ```json
    {
      "backend_servers": [{
        ...
        "domain": "other.com",
        ...
      }],
      ...
    }
    ```

3. (optional) SAML provider

    If you want to use SAML, you have to add "SAML" to the authentication_methods, and add the saml providers to the list
    The provider id needs to match the provider id from the settings.yaml:

    ```json
    {
      ...
      "authentication_methods": ["SAML"],
      "saml_provider": [{
        "title": "Some text before the button. e.g. Company Login",
        "provider_id": 1,
        "button_name": "SAML SSO Login"
      }]
      ...
    }
    ```

4. (optional) Create privacy policy

    Create your own privacy policy as a html file in e.g. `/opt/docker/psono-client/privacy-policy-content.html`

    If you are looking for inspiration you can check out our [privacy-policy-content.html](https://gitlab.com/psono/psono-client/blob/develop/src/common/data/privacy-policy-content.html)

5. Run the docker image and expose the port

    ```bash
    docker run --name psono-client \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/config.json \
        -v /opt/docker/psono-client/privacy-policy-content.html:/usr/share/nginx/html/privacy-policy-content.html \
        -d --restart=unless-stopped -p 10101:80 psono/psono-client:latest
    ```

    This will now start the Psono client on port 10101 with your config.json

    If you open now http://your-ip:10101 you should see a beautiful login screen.
    If you don't, please make sure no firewall is blocking your request.

    ::: tip
    Leave out the line with the privacy policy if you don't have your own
    :::

    ::: tip
    If you experience issues that the browser still uses the old `config.json`, check out our [FAQs here](/admin/faq/faq.html#psono-s-webclient-is-not-reflecting-the-changes-done-to-the-config-json-how-can-i-force-the-webclient-to-show-the-new-version) about how to solve that.
    :::

7. Setup Reverse Proxy

    To run the Psono password manager in production, a reverse proxy is needed, to handle the ssl offloading and glue the Psono server
    and webclient together. Follow the [guide to setup reverse proxy](/admin/installation/install-reverse-proxy.html) as a next step.

## Installation without Docker

The webclient is a pure html / js website, that can be hosted with any webserver and has zero dependencies.

1.  Download the webclient artifact

	Visit the following url and download the webclient:

	[get.psono.com/psono/psono-client/latest/webclient.zip](https://get.psono.com/psono/psono-client/latest/webclient.zip)

2.  Install webclient

    Unpack the webclient into the htdocs folder of your webserver.

3.  Update `config.json` with the following content:

	```json
	{
	  "backend_servers": [{
	    "title": "Psono.pw",
	    "url": "https://psono.example.com/server"
	  }],
	  "base_url": "https://psono.example.com/",
	  "allow_custom_server": true,
	  "allow_registration": true,
	  "allow_lost_password": true,
	  "disable_download_bar": false,
      "remember_me_default": false,
      "trust_device_default": false,
	  "authentication_methods": ["AUTHKEY", "LDAP"],
	  "saml_provider": []
	}
	```

	Adjust the title and URLs according to your setup. The backend_server url should be the url where you see the line below when you append /info/ to it:

	```
	{"info":"{\"version\": \"....}
	```

4.  (optional) Change domain for login

	You don't see it yet, but later in the login form your username will be a composition of something that the user
	defines, and a domain, forming the full username that looks similar to an email address (e.g. something@example.com).

	The client will parse the `base_url` parameter from the `config.json`. Sometimes you would want to specify a
	different domain. You can do that by adjusting the `config.json` as shown below:

	```json
	{
	  "backend_servers": [{
	    ...
	    "domain": "other.com",
	    ...
	  }],
	  ...
	}
	```

5.  (optional) SAML provider

	If you want to use SAML, you have to add "SAML" to the authentication_methods. You can remove LDAP and authkey, if
	you only want to support SAML. Afterwards add the saml providers to the list.
	The provider id needs to match the provider id from the settings.yaml:

	```json
	{
	  ...
      "authentication_methods": ["SAML"],
      "saml_provider": [{
        "title": "Some text before the button. e.g. Company Login",
        "provider_id": 1,
        "button_name": "SAML SSO Login"
      }]
	  ...
	}
	```

6.  (optional) Update privacy policy

	Update your privacy policy in `privacy-policy-content.html`

7.  Adjust permissions

    ```bash
    chmod -R 644 htdocs
    ```

	::: tip
	If you experience issues that the browser still uses the old `config.json`, check out our [FAQs here](/admin/faq/faq.html#psono-s-webclient-is-not-reflecting-the-changes-done-to-the-config-json-how-can-i-force-the-webclient-to-show-the-new-version) about how to solve that.
	:::

8.  Setup Reverse Proxy

	To run the Psono password manager in production, a reverse proxy is needed, to handle the ssl offloading and glue the Psono server
	and webclient together. Follow the [guide to setup reverse proxy](/admin/installation/install-reverse-proxy.html) as a next step.

