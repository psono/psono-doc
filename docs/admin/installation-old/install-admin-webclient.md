---
title: 4. Install Admin Webclient (optional)
metaTitle: Install Psono Admin Webclient | Psono Documentation
meta:
  - name: description
    content: Overview of how to use the API Key.
  - name: robots
    content: noindex, nofollow
---

# Install Admin Webclient

Installation guide of the Psono Admin Webclient

## Preamble

The admin webclient is a stateless website, allowing administrators to administrate Psono without the need to install anything
(besides a browser). It can be served by any web server, we prefer Nginx, but any other webserver (Apache, IIS) is fine too.
The admin web client is optional. As an alternative administrators can use the command line.

## Installation with Docker

The latest build of our Admin Web Client as a docker image can be found here: [hub.docker.com/r/psono/psono-admin-client/](https://hub.docker.com/r/psono/psono-admin-client/)
Follow belows instructions to bring it online.

1.  Enable Management API

	Verify that you have the following line in your server's' settings.yaml to enable the management API

    ```yaml
	MANAGEMENT_ENABLED: True
	```

    Don't forget to restart your server afterward.

2.  Promote your admin user

	As a next step you have to promote your user to become a superuser (at least for the first one).
	To do that, run the following command on one of your server instances:

	If you installed the server with docker:

    ```bash
    docker run --rm \
      -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-combo:latest python3 ./psono/manage.py promoteuser username@example.com superuser
    ```

3.  Run the docker image and expose the port

    ```bash
    docker run --name psono-admin-client \
        -v /opt/docker/psono-client/config.json:/usr/share/nginx/html/portal/config.json \
        -d --restart=unless-stopped -p 10102:80 psono/psono-admin-client:latest
    ```

	The `config.json` is the same one as you were using before with the normal client.

	This will now start the Psono admin client on port `10102`.

    If you open now `http://your-ip:10102` you should see a beautiful login screen.
    If you don't, please make sure no firewall is blocking your request.


4.  Setup Reverse Proxy

	To run the admin portal in production, a reverse proxy is needed, to handle the ssl offloading.
	Follow the [guide to setup reverse proxy](/admin/installation/install-reverse-proxy.html) as a next step.

## Installation without Docker

The admin client is a pure html / js website, that can be hosted with any webserver and has zero dependencies.

1.  Enable Management API

	Verify that you have the following line in your server's' settings.yaml to enable the management API

    ```yaml
	MANAGEMENT_ENABLED: True
	```

    Don't forget to restart your server afterward.


2.  Promote your admin user

	As a next step you have to promote your user to become a superuser (at least for the first one).
	To do that, run the following command on one of your server instances:

	If you installed the server with docker:

    ```bash
    docker run --rm \
      -v /opt/docker/psono/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-combo:latest python3 ./psono/manage.py promoteuser username@example.com superuser
    ```

3.  Download the admin artifact

	Visit the following url and download the admin webclient:

	[get.psono.com/psono/psono-admin-client/latest/adminclient.zip](https://get.psono.com/psono/psono-admin-client/latest/adminclient.zip)

4.  Install webclient

    Create a folder called `portal` in your htdocs directory of your webserver and unpack the webclient into that folder.

    ::: tip
    Other paths will not work.
    :::

5.  Update config.json

    In the `portal` folder you will find a config.json. You can copy the config.json from your normal web client and replace the one in the `portal` folder

    ::: tip
    Other paths will not work.
    :::
    
6.  Adjust permissions

    ```bash
    chmod -R 644 htdocs/portal
    ```

7.  Setup Reverse Proxy

	To run the admin portal in production, you now have to configure your webserver with SSL.
	Follow the [guide to setup reverse proxy](/admin/installation/install-reverse-proxy.html) as a next step.
