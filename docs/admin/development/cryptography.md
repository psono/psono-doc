---
title: Cryptography
metaTitle: Cryptography | Psono Documentation
meta:
  - name: description
    content: A brief overview of the used cryptography
---

# Cryptography

A brief overview of the used cryptography


## General

All cryptography is based on [https://nacl.cr.yp.to](https://nacl.cr.yp.to)

- **Secret Key cryptography** is based on: [XSalsa20](https://en.wikipedia.org/wiki/Salsa20) + [Poly1305](https://en.wikipedia.org/wiki/Poly1305)

- **Public Key cryptography** is based on: [Curve25519](https://en.wikipedia.org/wiki/Curve25519) + [XSalsa20](https://en.wikipedia.org/wiki/Salsa20) + [Poly1305](https://en.wikipedia.org/wiki/Poly1305)

- **Authkey derivation function** is based on [scrypt](https://en.wikipedia.org/wiki/Scrypt)

## Cryptography libraries

- Our web clients (websites and browser extensions) are using [ecma-nacl](https://github.com/3nsoft/ecma-nacl) a JavaScript implementation of NaCl.

- Our backends (server and fileserver) are using [PyNaCl](https://github.com/pyca/pynacl/)

- Our apps (Android and iOS) use are using [flutter_sodium](https://pub.dev/packages/flutter_sodium) and [Swift-Sodium](https://github.com/jedisct1/swift-sodium)


## Scrypt libraries

- The scrypt library used by our backends (server and fileserver) is [scrypt](https://pypi.python.org/pypi/scrypt/)

- The scrypt library used by our web clients (websites and browser extensions) is already part of [ecma-nacl](https://github.com/3nsoft/ecma-nacl).

- The scrypt library used by our apps (Android and iOS) is [pointycastle](https://pub.dev/packages/pointycastle)


## Authkey Algorithm

The authkey is generated with the following algorithm / scrypt parameters:

```javascript
var generate_authkey = function (username, password) {

    var salt = sha512(username.toLowerCase());

    var u = 14; // 2^14 = 16MB
    var r = 8;
    var p = 1;
    var l = 64; // 64 Bytes = 512 Bits

    var authkey = to_hex(scrypt(encode_utf8(password), salt, u, r, p, l));

    return authkey;
};
```

## Registration Process

The following diagram outlines the registration process, shows how the user's keys are generated and stored.

[![Registration Process](/images/Registration.png)](/images/Registration.png)

::: tip
Click on the diagram to zoom.
:::

## Login Process

The following diagram outlines the login process, shows how the signature of the server is checked, the authentication
key is generated and the whole session creation including multifactor challenges are handled.

[![Login Process](/images/Login.png)](/images/Login.png)

::: tip
Click on the diagram to zoom.
:::

## Secret: Create

The following diagram outlines the process how a secret e.g. a note or a website password entry is created, encrypted
and stored in the datastore of the user

[![Create secret process](/images/Create_Secret.png)](/images/Create_Secret.png)

::: tip
Click on the diagram to zoom.
:::

## Fileserver: File upload

The following diagram outlines the upload process to fileservers. It shows how a file is split up in chunks and the whole
communication between the server and the fileserver.

[![Fileserver upload process](/images/Fileupload.png)](/images/Fileupload.png)

::: tip
Click on the diagram to zoom.
:::

## Fileserver: File download

The following diagram outlines the download process to fileservers. It shows how the various chunks of a file are downloaded
decrypted and merged and the whole communication between the server and the fileserver.

[![Fileserver download process](/images/Filedownload.png)](/images/Filedownload.png)

::: tip
Click on the diagram to zoom.
:::

## Fileserver: File delete

The following diagram outlines the deletion process of files with fileservers. It shows the whole communication between the server and the fileserver.

[![Fileserver deletion process](/images/Filedelete.png)](/images/Filedelete.png)

::: tip
Click on the diagram to zoom.
:::


