---
title: Browser Extension
metaTitle: Development Browser Extension | Psono Documentation
meta:
  - name: description
    content: Everything around the development of the browser extension
---

# Browser Extension Development

Everything around the development of the browser extension. The browser extensions are nothing but some special packaged web clients, with some additional sugar for panels and so on.

## Preamble

This whole guide is based on Ubuntu 22.04 LTS. Other Ubuntu and Debian based systems should be similar if not even identical.

We assume that you already have somewhere a Psono client development environment up. If not follow the [guide to develop clients](/admin/development/webclient.html).

## Development

There are a lot of different ways to develop browser extensions. The most simple one is to create the unpackaged files
and then load them in the browser:

### Chrome

1.  Build

	To build the browser extension execute the following command:

		npm run buildchrome

2.  Follow this guide and load the extension from `psono-client/build/chrome`:

	[https://developer.chrome.com/docs/extensions/get-started/tutorial/hello-world#load-unpacked](https://developer.chrome.com/docs/extensions/get-started/tutorial/hello-world#load-unpacked)

	In short:

	1.  open Chrome
    2.  enter "chrome://extensions" in the URL bar
    3.  Ensure that the Developer mode checkbox in the top right-hand corner is checked.
    4.  click "Load unpacked extension…"
    5.  Navigate to `psono-client/build/chrome`, and select it.

### Firefox

1.  Build

	To build the browser extension execute the following command:

		npm run buildfirefox

2.  Follow this guide and load the extension from `psono-client/build/firefox`:

    [https://extensionworkshop.com/documentation/develop/temporary-installation-in-firefox/](https://extensionworkshop.com/documentation/develop/temporary-installation-in-firefox/)

	In short:

	1.  open Firefox
    2.  enter "about:debugging" in the URL bar
    3.  click "Load Temporary Add-on"
    4.  open `psono-client/build/firefox` and select any file inside.


## Release

### Chrome

The instruction will describe how to build / package and deploy the Chrome browser extension

1.  Build

	To build the browser extension execute the following command:

		npm run buildchrome


2.  Packaging

	First, we go into the build folder for the Chrome extension:

		cd psono-client/build/chrome/

	And zip everything:

		zip -r ../../chrome-extension.zip *

	And and lets go back up to the root of the project where our `chrome-extension.zip` is now located:

		cd ../..

3.  Deployment

	To deploy everything now to the Chrome Web Store execute the following:

		npm run deploychrome

	This will look for a `dist/chrome/psono.chrome.PW.zip` and deploy it. Replace the following variables:

		$webstore_client_id         # e.g.: 12345678-XXXXXXXXX...XXXX.apps.googleusercontent.com
		$webstore_client_secret     # e.g.: ABCDEFg-AaaaAAaaAKaasd9
		$webstore_refresh_token     # X/XXXXXxxxxXXXXX_XXxxxXxXXXxxx
		$webstore_app_id            # e.g.: eljmjmgjkbmpmfljlmklcfineebidmlo

More infos can be found here:

- [https://www.npmjs.com/package/webstore-upload](https://www.npmjs.com/package/webstore-upload)

- [https://developer.chrome.com/webstore/using_webstore_api](https://developer.chrome.com/webstore/using_webstore_api)


### Firefox

The instruction will describe how to build / package and deploy the Firefox browser extension

1.  Build

	To build the browser extension execute the following command:

		npm run buildfirefox

2.  Packaging

	First, we go into the build folder for the Firefox extension:

		cd psono-client/build/firefox/

	And zip everything:

		zip -r ../../firefox-extension.zip *

	And and lets go back up to the root of the project where our `firefox-extension.zip` is now located:

		cd ../..

3.  Deployment

	To deploy everything now to Firefox Add-ons execute the following:

		npm run deployfirefox

	This will look for a `firefox-extension.zip` and deploy it. Replace the following variables:

		$mozilla_jwt_issuer         # e.g.: user:12345689:123
		$mozilla_jwt_secret         # e.g.: asdasdasd45654334456asdasdasd6345645456asdasdsad456456456
		$mozilla_addon_id           # e.g.: {4cea82d7-5815-47eb-a7fe-2a5fd4974126}
		$mozilla_version      # e.g.: 1.2.3

More infos can be found here:

- [https://mozilla.github.io/addons-server/topics/api/v4_frozen/addons.html](https://mozilla.github.io/addons-server/topics/api/v4_frozen/addons.html)


