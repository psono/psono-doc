---
title: Webclient
metaTitle: Development Webclient | Psono Documentation
meta:
  - name: description
    content: Getting started guide for the webclient development
---

# Webclient Development

If you want to start to develop own features for the clients, then follow the following steps to setup your own development environment.

## Preamble

This whole guide is based on Ubuntu 22.04 LTS. Other Ubuntu and Debian based systems should be similar if not even identical.

We assume that you already have somewhere a Psono server running. If not follow the [guide to setup a Psono server](/admin/development/datastore-structure.html).

## Installation

1. Install some generic stuff

    ```bash
    sudo apt-get update
    sudo apt-get install -y git
    ```

2. Clone git repository

    ```bash
    git clone https://gitlab.com/psono/psono-client.git ~/psono-client
    ```

3. Checkout new branch

    ```bash
    cd ~/psono-client
    git fetch
    git checkout develop
    git checkout -b [name_of_your_new_branch]
    ```

4. Install requirements

    This step will install node and npm and all the npm packages.

    ```bash
    sudo var/prep-build.sh
    ```

## Run the dev server

From this point on forward, you can develop it like any web application.

To start the dev server with the web client run the following command:

```bash
num run dev
```

## Build everything

To build the webclient and browser extensions, use the following three command:

```bash
npm run buildchrome
npm run buildfirefox
npm run buildwebclient
```

## Run Unit Tests

Issue the following command:

```bash
npm test
```


