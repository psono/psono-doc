import { getAssetFromKV, mapRequestToAsset } from '@cloudflare/kv-asset-handler'

/**
 * The DEBUG flag will do two things that help during development:
 * 1. we will skip caching on the edge, which makes it easier to
 *    debug.
 * 2. we will return an error message on exception in your Response rather
 *    than the default 404.html page.
 */
const DEBUG = false

addEventListener('fetch', event => {
  try {
    event.respondWith(handleEvent(event))
  } catch (e) {
    if (DEBUG) {
      return event.respondWith(
        new Response(e.message || e.toString(), {
          status: 500,
        }),
      )
    }
    event.respondWith(new Response('Internal Error', { status: 500 }))
  }
})

async function handleEvent(event) {
  const url = new URL(event.request.url)
  let options = {}

  /**
   * You can add custom logic to how we fetch your assets
   * by configuring the function `mapRequestToAsset`
   */
  // options.mapRequestToAsset = handlePrefix(/^\/docs/)

  if (url.pathname === '/a.js') {
    let data = '';


    const euCountries= [
      // countries with the Euro
      'AT',
      'BE',
      'CH',
      'CY',
      'CY',
      'DE',
      'ES',
      'FI',
      'FR',
      'GR',
      'HR',
      'IE',
      'IT',
      'LV',
      'LT',
      'LU',
      'MT',
      'NL',
      'PT',
      'SK',
      'SI',
      // countries in the EU without the Euro
      'BG',
      'CZ',
      'HU',
      'PL',
      'RO',
      'SE',
      'DK',
    ]

    if (
        !euCountries.includes(event.request.cf.country)
    ) {

      // data += 'window.dataLayer = window.dataLayer || [];\n' +
      //     'function gtag(){dataLayer.push(arguments);}' +
      //     'gtag("consent", "default", {\n' +
      //     '    ad_storage: "denied",\n' +
      //     '    analytics_storage: "denied"\n' +
      //     '    functionality_storage: "denied",\n' +
      //     '    personalization_storage: "denied",\n' +
      //     '    security_storage: "denied"\n,' +
      //     '    region: [\n' +
      //     '    "AT",\n' +
      //     '    "BE",\n' +
      //     '    "CH",\n' +
      //     '    "CY",\n' +
      //     '    "CY",\n' +
      //     '    "DE",\n' +
      //     '    "ES",\n' +
      //     '    "FI",\n' +
      //     '    "FR",\n' +
      //     '    "GR",\n' +
      //     '    "HR",\n' +
      //     '    "IE",\n' +
      //     '    "IT",\n' +
      //     '    "LV",\n' +
      //     '    "LT",\n' +
      //     '    "LU",\n' +
      //     '    "MT",\n' +
      //     '    "NL",\n' +
      //     '    "PT",\n' +
      //     '    "SK",\n' +
      //     '    "SI",\n' +
      //     '    "BG",\n' +
      //     '    "CZ",\n' +
      //     '    "HU",\n' +
      //     '    "PL",\n' +
      //     '    "RO",\n' +
      //     '    "SE",\n' +
      //     '    "DK"\n' +
      //     ']' +
      //     '});\n';
      //data += 'gtag("set", "ads_data_redaction", true);';

      data += '(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':\n' +
          'new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],\n' +
          'j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=\n' +
          '\'https://a.esaqa.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);\n' +
          '})(window,document,\'script\',\'dataLayer\',\'GTM-KXGFVWST\');';
    }

    return new Response(data, {
      headers: {
        'content-type': 'text/javascript;charset=UTF-8',
      },
    });
  }


  try {
    if (DEBUG) {
      // customize caching
      options.cacheControl = {
        bypassCache: true,
      }
    }
    return await getAssetFromKV(event, options)
  } catch (e) {
    // if an error is thrown try to serve the asset at 404.html
    if (!DEBUG) {
      try {
        let notFoundResponse = await getAssetFromKV(event, {
          mapRequestToAsset: req => new Request(`${new URL(req.url).origin}/404.html`, req),
        })

        return new Response(notFoundResponse.body, { ...notFoundResponse, status: 404 })
      } catch (e) {}
    }

    return new Response(e.message || e.toString(), { status: 500 })
  }
}

/**
 * Here's one example of how to modify a request to
 * remove a specific prefix, in this case `/docs` from
 * the url. This can be useful if you are deploying to a
 * route on a zone, or if you only want your static content
 * to exist at a specific path.
 */
function handlePrefix(prefix) {
  return request => {
    // compute the default (e.g. / -> index.html)
    let defaultAssetKey = mapRequestToAsset(request)
    let url = new URL(defaultAssetKey.url)

    // strip the prefix from the path for lookup
    url.pathname = url.pathname.replace(prefix, '/')

    // inherit all other props from the default request
    return new Request(url.toString(), defaultAssetKey)
  }
}